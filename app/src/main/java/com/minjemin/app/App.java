package com.minjemin.app;

import android.app.Application;

import com.minjemin.app.data.AppState;
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        AppState.getInstance().initSharedPrefs(this);

    }
}
