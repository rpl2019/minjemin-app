package com.minjemin.app.models;

import com.google.gson.annotations.SerializedName;

public class OrderStatus {

    @SerializedName("order_id")
    private String orderId;
    private String name;
    @SerializedName("is_complete")
    private int isComplete;
    @SerializedName("created_at")
    private String time;

    public OrderStatus(String orderId, String name, int isComplete, String time) {
        this.orderId = orderId;
        this.name = name;
        this.isComplete = isComplete;
        this.time = time;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIsComplete() {
        return isComplete;
    }

    public void setIsComplete(int isComplete) {
        this.isComplete = isComplete;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
