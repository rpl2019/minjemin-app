package com.minjemin.app.models;

import android.view.View;

public class ProfileSubMenuItem {
    private String title;
    private View.OnClickListener onClick;

    public ProfileSubMenuItem(String title, View.OnClickListener onClick) {
        this.title = title;
        this.onClick = onClick;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public View.OnClickListener getOnClick() {
        return onClick;
    }
}
