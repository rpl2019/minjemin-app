package com.minjemin.app.models;

import android.view.View;

public class Alamat {
    String labelAlamat,nama,alamatLengkap,kab,kec,prov,kodePos,noTelp;
    private View.OnClickListener onClick;


    public Alamat(String labelAlamat, String nama, String alamatLengkap, String kab, String kec, String prov, String kodePos, String noTelp) {
        this.labelAlamat = labelAlamat;
        this.nama = nama;
        this.alamatLengkap = alamatLengkap;
        this.kab = kab;
        this.kec = kec;
        this.prov = prov;
        this.kodePos = kodePos;
        this.noTelp = noTelp;
    }

    public String getLabelAlamat() {
        return labelAlamat;
    }

    public void setLabelAlamat(String labelAlamat) {
        this.labelAlamat = labelAlamat;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamatLengkap() {
        return alamatLengkap;
    }

    public void setAlamatLengkap(String alamatLengkap) {
        this.alamatLengkap = alamatLengkap;
    }

    public String getKab() {
        return kab;
    }

    public void setKab(String kab) {
        this.kab = kab;
    }

    public String getKec() {
        return kec;
    }

    public void setKec(String kec) {
        this.kec = kec;
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public String getKodePos() {
        return kodePos;
    }

    public void setKodePos(String kodePos) {
        this.kodePos = kodePos;
    }

    public String getNoTelp() {
        return noTelp;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }
    public View.OnClickListener getOnClick() {
        return onClick;
    }
}
