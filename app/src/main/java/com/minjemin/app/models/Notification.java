package com.minjemin.app.models;

import android.view.View;

public class Notification {
    String title,description,date,time;
    private View.OnClickListener onClick;
    public Notification(String title, String description, String date, String time, View.OnClickListener onClick) {
        this.title = title;
        this.description = description;
        this.date = date;
        this.time = time;
        this.onClick = onClick;
    }

    public View.OnClickListener getOnClick() {
        return onClick;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
