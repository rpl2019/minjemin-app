package com.minjemin.app.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Invoice {

    private String id;
    private String number;
    private User renter;
    private Item item;
    private String amount;
    @SerializedName("amount_format")
    private String amountFormat;
    @SerializedName("start_date")
    private String startDate;
    @SerializedName("end_date")
    private String endDate;
    private String address;
    private Payment payment;
    private List<OrderStatus> status;

    public Invoice() {
    }

    public Invoice(String id, String number, User renter, Item item, String amount,
                   String amountFormat, String startDate, String endDate, String address,
                   Payment payment, List<OrderStatus> status) {
        this.id = id;
        this.number = number;
        this.renter = renter;
        this.item = item;
        this.amount = amount;
        this.amountFormat = amountFormat;
        this.startDate = startDate;
        this.endDate = endDate;
        this.address = address;
        this.payment = payment;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public User getRenter() {
        return renter;
    }

    public void setRenter(User renter) {
        this.renter = renter;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmountFormat() {
        return amountFormat;
    }

    public void setAmountFormat(String amountFormat) {
        this.amountFormat = amountFormat;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public List<OrderStatus> getStatus() {
        return status;
    }

    public void setStatus(List<OrderStatus> status) {
        this.status = status;
    }
}
