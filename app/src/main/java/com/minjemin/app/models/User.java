package com.minjemin.app.models;

import com.google.gson.annotations.SerializedName;

public class User {

    private String id;
    private String username;
    private String avatar;
    private String email;
    private String name;
    private String gender;
    private String phone;
    private String address;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public class ErrorChangePassword {
        String currentPassword;
        String newPassword;
        String confirmPassword;

        public String getCurrentPassword() {
            return currentPassword;
        }

        public void setCurrentPassword(String currentPassword) {
            this.currentPassword = currentPassword;
        }

        public String getNewPassword() {
            return newPassword;
        }

        public void setNewPassword(String newPassword) {
            this.newPassword = newPassword;
        }

        public String getConfirmPassword() {
            return confirmPassword;
        }

        public void setConfirmPassword(String confirmPassword) {
            this.confirmPassword = confirmPassword;
        }

        public void clear() {
            setCurrentPassword(null);
            setNewPassword(null);
            setConfirmPassword(null);
        }
    }


    public class ErrorRegister {
        String name;
        String username;
        String email;
        String password;
        String confirmationPassword;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getConfirmationPassword() {
            return confirmationPassword;
        }

        public void setConfirmationPassword(String confirmationPassword) {
            this.confirmationPassword = confirmationPassword;
        }

        public void clear() {
            setName(null);
            setUsername(null);
            setEmail(null);
            setPassword(null);
            setConfirmationPassword(null);
        }
    }

    public ErrorChangePassword initErrorChangePassword() {
        return new ErrorChangePassword();
    }
    public ErrorRegister initErrorRegister() { return new ErrorRegister(); }
}
