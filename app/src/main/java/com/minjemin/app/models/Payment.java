package com.minjemin.app.models;

import com.google.gson.annotations.SerializedName;

public class Payment {

    private String id;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("bank_id")
    private String bankId;
    @SerializedName("order_number")
    private String orderNumber;
    @SerializedName("account_number")
    private String accountNumber;
    @SerializedName("account_name")
    private String accountName;
    private String amount;
    @SerializedName("image_url")
    private String imageUrl;
    @SerializedName("created_at")
    private String timestamp;

    public Payment() {}
    public Payment(String id, String userId, String bankId, String orderNumber,
                   String accountNumber, String accountName, String amount, String imageUrl,
                   String timestamp) {
        this.id = id;
        this.userId = userId;
        this.bankId = bankId;
        this.orderNumber = orderNumber;
        this.accountNumber = accountNumber;
        this.accountName = accountName;
        this.amount = amount;
        this.imageUrl = imageUrl;
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
