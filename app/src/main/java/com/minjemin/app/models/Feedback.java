package com.minjemin.app.models;

import com.google.gson.annotations.SerializedName;

public class Feedback {
    private String id;
    @SerializedName("user_id")
    private String userId;
    private Item item;
    private String rating;
    private String comment;
    private String created_at;

    public Feedback() {
    }

    public Feedback(String id, String userId, Item item, String rating, String comment, String created_at) {
        this.id = id;
        this.userId = userId;
        this.item = item;
        this.rating = rating;
        this.comment = comment;
        this.created_at = created_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
