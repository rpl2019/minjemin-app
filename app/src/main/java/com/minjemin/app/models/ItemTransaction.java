package com.minjemin.app.models;

public class ItemTransaction {

    private Item item;
    private String remain;
    private String status;

    public ItemTransaction() {
    }

    public ItemTransaction(Item item, String remain, String status) {
        this.item = item;
        this.remain = remain;
        this.status = status;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public String getRemain() {
        return remain;
    }

    public void setRemain(String remain) {
        this.remain = remain;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
