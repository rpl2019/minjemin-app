package com.minjemin.app.models;

import com.google.gson.annotations.SerializedName;

public class Returning {

    private String id;
    @SerializedName("order_id")
    private String orderId;
    @SerializedName("created_at")
    private String timestamp;

    public Returning(String id, String orderId, String timestamp) {
        this.id = id;
        this.orderId = orderId;
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
