package com.minjemin.app.models;


import android.view.View;

public class ProfileMenuItem {

    private String title;
    private int icon;
    private View.OnClickListener onClick;

    public ProfileMenuItem(String title, int icon, View.OnClickListener onClick) {
        this.title = title;
        this.icon = icon;
        this.onClick = onClick;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public View.OnClickListener getOnClick() {
        return onClick;
    }


    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }


}
