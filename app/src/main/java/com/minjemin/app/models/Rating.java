package com.minjemin.app.models;

import com.google.gson.annotations.SerializedName;

public class Rating {
    @SerializedName("average_rate")
    private String averageRate;
    @SerializedName("user_count")
    private int userCount;

    public Rating(String averageRate, int userCount) {
        this.averageRate = averageRate;
        this.userCount = userCount;
    }

    public String getAverageRate() {
        return averageRate;
    }

    public void setAverageRate(String averageRate) {
        this.averageRate = averageRate;
    }

    public int getUserCount() {
        return userCount;
    }

    public void setUserCount(int userCount) {
        this.userCount = userCount;
    }
}
