package com.minjemin.app.screen.vendor.item.extra;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.minjemin.app.R;
import com.minjemin.app.base.BaseRecycleAdapter;
import com.minjemin.app.models.Category;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryAdapter extends BaseRecycleAdapter<Category> {

    private final static int TYPE = 1;
    private ItemListener mListener;

    public CategoryAdapter(Context context, ArrayList<Category> items, ItemListener listener) {
        super(context, items);
        this.mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder setViewHolder(ViewGroup parent, @Nullable int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, null);
        return new Holder(view);
    }

    @Override
    public void onBindData(RecyclerView.ViewHolder holder, Category val) {
        Holder viewHolder = (Holder) holder;
        viewHolder.bindTo(val);
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.text_category)
        TextView textCat;
        String name;
        String id;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        void bindTo(Category cat) {
            this.id = cat.getId();
            this.name = cat.getName();
            textCat.setText(cat.getName());
        }

        @Override
        public void onClick(View v) {
            if(mListener != null) {
                mListener.onItemClick(name, id);
            }
        }
    }

    public interface ItemListener {
        void onItemClick(String name, String id);
    }
}
