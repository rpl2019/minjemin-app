package com.minjemin.app.screen.pengembalian;

import android.util.Log;

import com.minjemin.app.base.BaseErrorResponse;
import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.utils.ErrorUtils;
import com.minjemin.app.utils.StatedViewModel;
import com.minjemin.app.utils.UtilsApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PengembalianViewModel extends StatedViewModel<PengembalianViewModel.State> {

    private BaseApiService mApiService = UtilsApi.getApiService();

    @Override
    protected State initState() {
        return new State();
    }

    class State {
        boolean isLoading = false;
        List<String> courierList = new ArrayList<>();
        Status status = Status.INIT;
        String message = "";
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    void setupCourier() {
        state.courierList.add("Diantar Langsung");
//        state.courierList.add(new Courier("2", "JNE"));
//        state.courierList.add(new Courier("2", "J&T"));
        updateState();
    }

    void doReturn(String id, String address) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.returnItem(id, address)
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.message = response.body().getMessage();
                            state.status = Status.SUCCESS;
                            Log.d("RETURNN", response.body().getMessage());
                        } else {
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.message = errorResponse.getMessage();
                            state.status = Status.ERROR;
                            Log.d("RETURNN", errorResponse.getMessage());
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        state.message = t.getMessage();
                        Log.d("RETURNN", t.getMessage());
                        updateState();
                    }
                });
    }
}
