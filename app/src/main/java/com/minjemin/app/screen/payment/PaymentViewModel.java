package com.minjemin.app.screen.payment;

import com.minjemin.app.base.BaseErrorResponse;
import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.utils.ErrorUtils;
import com.minjemin.app.utils.StatedViewModel;
import com.minjemin.app.utils.UtilsApi;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentViewModel extends StatedViewModel<PaymentViewModel.State> {

    private BaseApiService mApiService = UtilsApi.getApiService();

    @Override
    protected State initState() {
        return new State();
    }

    class State {
        boolean isLoading = false;
        Status status = Status.INIT;
        String message = "";
    }

    enum Status {
        INIT, SUCCESS, ERROR;
    }

    public void doPayment(String orderNumber, String bankName, String accNumber, String accName,
                          String amount, MultipartBody.Part image) {


        RequestBody orderNo = RequestBody.create(MediaType.parse("text/plain"), orderNumber);
        RequestBody bankId = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), bankName);
        RequestBody accNo = RequestBody.create(MediaType.parse("text/plain"), accNumber);
        RequestBody accNama = RequestBody.create(MediaType.parse("text/plain"), accName);
        RequestBody orderAmount = RequestBody.create(MediaType.parse("text/plain"), amount);

        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.uploadTransfer(bankId, orderNo, name, accNo, accNama, orderAmount, image)
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.status = Status.SUCCESS;
                            state.message = response.body().getMessage();
                        } else {
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.status = Status.ERROR;
                            state.message = errorResponse.getMessage();
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        state.message = t.getMessage();
                        updateState();

                    }
                });

    }
}
