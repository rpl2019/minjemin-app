package com.minjemin.app.screen.main.profile.notification;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.minjemin.app.R;
import com.minjemin.app.base.BaseRecycleAdapter;
import com.minjemin.app.models.Notification;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends BaseRecycleAdapter<Notification> {
    public NotificationAdapter(Context context, ArrayList<Notification> items) {
        super(context, items);
    }

    @Override
    public RecyclerView.ViewHolder setViewHolder(ViewGroup parent, @Nullable int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_notification, parent, false);
        return new NotificationAdapter.Holder(view);
    }

    @Override
    public void onBindData(RecyclerView.ViewHolder holder, Notification val) {
        NotificationAdapter.Holder viewHolder = (NotificationAdapter.Holder) holder;
        viewHolder.bindTo(val);
    }

    class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_title)
        TextView textTitle;
        @BindView(R.id.text_description)
        TextView textDesc;
        @BindView(R.id.text_date)
        TextView textDate;
        @BindView(R.id.text_time)
        TextView textTime;
        private Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindTo(Notification item) {
            textTitle.setText(item.getTitle());
            textDesc.setText(item.getDescription());
            textDate.setText(item.getDate());
            textTime.setText(item.getTime());
            itemView.setOnClickListener(item.getOnClick());

        }
    }
}
