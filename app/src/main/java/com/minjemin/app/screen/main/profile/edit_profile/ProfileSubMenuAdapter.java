package com.minjemin.app.screen.main.profile.edit_profile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.minjemin.app.R;
import com.minjemin.app.base.BaseRecycleAdapter;
import com.minjemin.app.models.ProfileSubMenuItem;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileSubMenuAdapter extends BaseRecycleAdapter<ProfileSubMenuItem> {
    public ProfileSubMenuAdapter(Context context, ArrayList<ProfileSubMenuItem> items) {
        super(context, items);
    }

    @Override
    public RecyclerView.ViewHolder setViewHolder(ViewGroup parent, @Nullable int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_sub_profile, parent, false);
        return new ProfileSubMenuAdapter.Holder(view);
    }

    @Override
    public void onBindData(RecyclerView.ViewHolder holder, ProfileSubMenuItem val) {
        ProfileSubMenuAdapter.Holder viewHolder = (ProfileSubMenuAdapter.Holder) holder;
        viewHolder.bindTo(val);
    }

    class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_place)
        TextView title;


        private Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindTo(ProfileSubMenuItem item) {
            title.setText(item.getTitle());
            itemView.setOnClickListener(item.getOnClick());
        }
    }
}
