package com.minjemin.app.screen.main.profile;

import com.minjemin.app.data.AppState;
import com.minjemin.app.models.ProfileMenuItem;
import com.minjemin.app.models.User;
import com.minjemin.app.utils.StatedViewModel;

import java.util.ArrayList;

public class ProfileViewModel extends StatedViewModel<ProfileViewModel.State> {

    private User currentUser;
    private ProfileAdapter adapter;
    private ArrayList<ProfileMenuItem> menuList = new ArrayList<>();

    @Override
    protected State initState() {
        return new State();
    }

    public class State {
        boolean isLoading = false;
    }


    public boolean loadUser() {
        currentUser = AppState.getInstance().getUser();
        if (currentUser != null) {
            return true;
        }
        return false;
    }

    public void setMenu(ArrayList<ProfileMenuItem> list) {
        this.menuList = list;

    }
    public ArrayList<ProfileMenuItem> getMenu() {
        return menuList;
    }

    public ProfileAdapter getAdapter() {
        return adapter;
    }


    public User getUser() {
        return currentUser;
    }

}
