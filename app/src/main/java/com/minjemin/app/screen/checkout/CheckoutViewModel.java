package com.minjemin.app.screen.checkout;

import android.util.Log;

import com.minjemin.app.base.BaseErrorResponse;
import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.models.Item;
import com.minjemin.app.models.Order;
import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.screen.item.ItemDetailViewModel;
import com.minjemin.app.utils.ErrorUtils;
import com.minjemin.app.utils.StatedViewModel;
import com.minjemin.app.utils.UtilsApi;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutViewModel extends StatedViewModel<CheckoutViewModel.State> {

    private BaseApiService mApiService = UtilsApi.getApiService();

    @Override
    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        Item item;
        String message;
        String availability;
        String orderMessage;
        boolean isLoading = false;
        Status status = Status.INIT;
        Status statusAva = Status.INIT;
        Status statusOrder = Status.INIT;
        Order order;
    }

    void doGetItem(String id) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();

        mApiService.getItem(id)
                .enqueue(new Callback<BaseResponse<Item>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<Item>> call,
                                           Response<BaseResponse<Item>> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.item = response.body().getData();
                            state.status = Status.SUCCESS;
                        } else {
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.status = Status.ERROR;
                            state.message = errorResponse.getMessage();
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<Item>> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        state.message = t.getMessage();
                        updateState();
                    }
                });

    }

    void doCheckOrderExists(String id, String fromDate, String toDate) {
        state.isLoading = true;
        state.statusAva = Status.INIT;
        updateState();

        mApiService.requestCheckOrder(id, fromDate, toDate)
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.statusAva = Status.SUCCESS;
                            state.availability = response.body().getMessage();

                        } else {
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.statusAva = Status.ERROR;
                            state.availability = errorResponse.getMessage();
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        state.isLoading = false;
                        state.statusAva = Status.ERROR;
                        state.availability = t.getMessage();
                        updateState();
                    }
                });

    }

    void doStoreOrder(String id, String fromDate, String toDate, String address) {
        state.isLoading = true;
        state.statusOrder = Status.INIT;
        updateState();
        mApiService.requestOrder(id, fromDate, toDate, address)
                .enqueue(new Callback<BaseResponse<Order>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<Order>> call, Response<BaseResponse<Order>> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.statusOrder = Status.SUCCESS;
                            state.orderMessage = response.body().getMessage();
                            state.order = response.body().getData();
                            Log.d("ORDERR", response.body().getData().getNumber());
                            Log.d("ORDERR", "DISINI BERHASIL");
                        } else {
                            state.statusOrder = Status.ERROR;
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.orderMessage = errorResponse.getMessage();
                            Log.d("ORDERR", "DISINI ERROR");
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<Order>> call, Throwable t) {
                        state.isLoading = false;
                        state.statusOrder = Status.ERROR;
                        state.orderMessage = t.getMessage();
                        Log.d("ORDERR", "DISINI ERROR 2");
                        Log.d("ORDERR", t.getMessage());
                        updateState();
                    }
                });
    }
}
