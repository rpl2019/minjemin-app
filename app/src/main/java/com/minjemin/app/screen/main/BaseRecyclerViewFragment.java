package com.minjemin.app.screen.main;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.minjemin.app.R;
import com.minjemin.app.base.BaseRecyclerAdapter;
import com.minjemin.app.utils.StatedViewModel;

public abstract class BaseRecyclerViewFragment<T extends StatedViewModel, S> extends BaseStatedFragment<T, S> {
    public BaseRecyclerAdapter adapter;
    RecyclerView recyclerItem;

    @Override
    Integer getLayout() {
        return R.layout.main_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new BaseRecyclerAdapter();
        recyclerItem.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerItem.setAdapter(adapter);
    }
}
