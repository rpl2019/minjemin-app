package com.minjemin.app.screen.main.profile.edit_alamat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.minjemin.app.R;
import com.minjemin.app.base.BaseRecycleAdapter;
import com.minjemin.app.models.Alamat;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class EditAlamatAdapter extends BaseRecycleAdapter<Alamat> {

    public EditAlamatAdapter(Context context, ArrayList<Alamat> items) {
        super(context, items);
    }

    @Override
    public RecyclerView.ViewHolder setViewHolder(ViewGroup parent, @Nullable int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_alamat, parent, false);
        return new EditAlamatAdapter.Holder(view);
    }

    @Override
    public void onBindData(RecyclerView.ViewHolder holder, Alamat val) {

    }


    class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.label_alamat)
        TextView labelAlamat;
        @BindView(R.id.label_nama)
        TextView labelNama;
        @BindView(R.id.text_alamat_lengkap)
        TextView textAlamatLengkap;
        @BindView(R.id.text_kecamatan)
        TextView textKecamatan;
        @BindView(R.id.text_kabupaten)
        TextView textKabupaten;
        @BindView(R.id.text_provinsi)
        TextView textProvinsi;
        @BindView(R.id.text_kode_pos)
        TextView textKodePos;
        @BindView(R.id.text_no_telp)
        TextView textNoTelp;
        @BindView(R.id.btn_edit)
        Button btnEdit;



        private Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindTo(Alamat item) {
            labelAlamat.setText(item.getLabelAlamat());
            labelNama.setText(item.getNama());
            textAlamatLengkap.setText(item.getAlamatLengkap());
            textKecamatan.setText(item.getKec());
            textKabupaten.setText(item.getKab());
            textProvinsi.setText(item.getProv());
            textKodePos.setText(item.getKodePos());
            textNoTelp.setText(item.getNoTelp());

            itemView.setOnClickListener(item.getOnClick());
        }
    }
}
