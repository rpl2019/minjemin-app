package com.minjemin.app.screen.transaksi;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.minjemin.app.R;
import com.minjemin.app.screen.transaksi.peminjaman.PeminjamanFragment;
import com.minjemin.app.screen.transaksi.tagihan.TagihanFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TransaksiFragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.pager_transaksi)
    ViewPager viewPager;
    @BindView(R.id.tabs_transaksi)
    TabLayout tabs;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_transaksi_peminjaman, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewPager.setAdapter(new TransactionPagerAdapter(getChildFragmentManager(), 2));
        tabs.setupWithViewPager(viewPager);
    }

    class TransactionPagerAdapter extends FragmentStatePagerAdapter {
        private int mNumTabs;

        public TransactionPagerAdapter(@NonNull FragmentManager fm, int mNumTabs) {
            super(fm);
            this.mNumTabs = mNumTabs;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.pager_tagihan);
                case 1:
                    return getString(R.string.pager_peminjaman);
                default:
                    return null;

            }
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return TagihanFragment.instance();
                case 1:
                    return PeminjamanFragment.instance();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumTabs;
        }
    }
}
