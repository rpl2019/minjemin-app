package com.minjemin.app.screen.main.profile.edit_profile;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.minjemin.app.R;
import com.minjemin.app.data.AppState;
import com.minjemin.app.models.User;

import java.io.File;
import java.io.IOException;

public class EditProfileActivity extends AppCompatActivity {

    @BindView(R.id.image_profile)
    CircleImageView avatar;
    @BindView(R.id.textName)
    EditText textName;
    @BindView(R.id.textUsername)
    EditText textUsername;
    @BindView(R.id.textEmail)
    EditText textEmail;
    @BindView(R.id.textPhone)
    EditText textPhone;
    @BindView(R.id.radioMale)
    RadioButton radioMale;
    @BindView(R.id.radioFemale)
    RadioButton radioFemale;
    @BindView(R.id.button_save_profile)
    Button buttonSave;
    @BindView(R.id.radioGender)
    RadioGroup radioGender;
    @BindView(R.id.text_change_photo)
    TextView changeAvatar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.image_refresh)
    ImageView mRefresh;

    private EditProfileViewModel viewModel;
    private Context mContext;
    private ImagePicker imagePicker;
    private Image tmpImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Edit Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = this;
        init();
    }

    private void init() {
        textUsername.setEnabled(false);
        viewModel = ViewModelProviders.of(this).get(EditProfileViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderProfile(state.currentUser);
            renderLoading(state.isLoading);
            renderMessage(state.message);
        });

        avatar.setOnClickListener(v -> {
            getImagePicker().start();
        });

        saveChanges();

        mRefresh.setOnClickListener(v -> {
            viewModel.doGetUser();
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void saveChanges() {
        buttonSave.setOnClickListener(v -> {
            String gender = null;
            switch (radioGender.getCheckedRadioButtonId()) {
                case R.id.radioMale: {
                    gender = "L";
                    break;
                }
                case R.id.radioFemale: {
                    gender = "P";
                    break;
                }
            }
            viewModel.doUpdateProfile(AppState.getInstance().getUser().getId(),
                    textName.getText().toString(), textEmail.getText().toString(), gender, textPhone.getText().toString());
        });
    }

    private void renderProfile(User user) {
        Glide.with(mContext).load(user.getAvatar()).fitCenter().into(avatar);
        textName.setText(user.getName());
        textUsername.setText(user.getUsername());
        textEmail.setText(user.getEmail());
        textPhone.setText(user.getPhone());
        if (user.getGender().equals("L")) {
            radioMale.setChecked(true);
        } else {
            radioFemale.setChecked(true);
        }
    }

    private void renderLoading(boolean isLoading) {
        progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    private void renderMessage(String msg) {
        if (!msg.equals("")) {
            Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
            viewModel.getState().getValue().message = "";
        }
    }

    public ImagePicker getImagePicker() {
        imagePicker = ImagePicker.create(this)
                .theme(R.style.ImagePickerTheme)
                .toolbarImageTitle("Tap to select")
                .toolbarDoneButtonText("DONE")
                .folderMode(true)
                .single();

        return imagePicker.limit(1)
                .showCamera(true)
                .imageDirectory("camera")
                .imageFullDirectory(Environment.getExternalStorageDirectory().getPath());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            tmpImage = ImagePicker.getFirstImageOrNull(data);
            uploadImage(tmpImage);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void uploadImage(Image image) {
        if (image == null) return;

        File file = new File(image.getPath());
        File compresedImg = null;
        try {
            compresedImg = new Compressor(mContext).compressToFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), compresedImg);
        MultipartBody.Part img = MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);
        viewModel.doUpdateAvatar(img);
    }

}
