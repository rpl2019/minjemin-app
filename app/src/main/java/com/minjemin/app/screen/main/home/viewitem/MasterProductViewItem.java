package com.minjemin.app.screen.main.home.viewitem;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.minjemin.app.R;
import com.minjemin.app.base.BaseRecyclerAdapter;
import com.minjemin.app.base.ViewItem;
import com.minjemin.app.models.Home;
import com.minjemin.app.models.Item;
import com.minjemin.app.utils.ViewTypeGenerator;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MasterProductViewItem extends ViewItem<MasterProductViewItem.Holder> {

    private Home<Item> mItem;

    public MasterProductViewItem(Home<Item> mItem) {
        this.mItem = mItem;
    }

    @Override
    public int getViewType() {
        return new ViewTypeGenerator(getClass().getName()).getHasCode();
    }

    @Override
    public Holder createHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, null);
        return new Holder(view);
    }

    @Override
    public void bindHolder(Holder holder) {
        holder.bind(mItem);
    }

    class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.sub_title)
        TextView subTitle;
        @BindView(R.id.view_all)
        TextView viewAll;
        @BindView(R.id.rv_item)
        RecyclerView recyclerView;
        private BaseRecyclerAdapter adapter;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Home<Item> val) {
            subTitle.setText(val.getTitle());
            adapter = new BaseRecyclerAdapter();
            adapter.clear();

            for (Item item : val.getItemList()) {
                adapter.addItems(new ProductViewItem(item));
            }
            recyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext(),
                    LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setAdapter(adapter);
            viewAll.setOnClickListener(v -> {
                Toast.makeText(itemView.getContext(), "You're Clicked "
                        + val.getTitle() + " ViewAll", Toast.LENGTH_SHORT).show();
            });
        }

    }
}
