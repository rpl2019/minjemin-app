package com.minjemin.app.screen.main.profile.edit_alamat;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.minjemin.app.R;
import com.minjemin.app.models.Alamat;

import java.util.ArrayList;

public class EditAlamatActivity extends AppCompatActivity {
    @BindView(R.id.toolbar_edit_alamat)
    Toolbar toolbar;
    @BindView(R.id.title)
    TextView title;
    TextView textTambah;
    Button btnEdit;
    @BindView(R.id.rv_edit_alamat)
    RecyclerView recyclerView;


    private RecyclerView.LayoutManager layoutManager;
    private EditAlamatAdapter adapter;
    private ArrayList<Alamat> itemAlamat;

    private Alamat mViewModel;
    private Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_alamat);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(title.getText().toString());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mContext = recyclerView.getContext();
        itemAlamat = new ArrayList<>();
        setUp();
        adapter = new EditAlamatAdapter(mContext,itemAlamat);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(adapter);
        btnEdit = findViewById(R.id.btn_edit);
        textTambah = findViewById(R.id.text_tambah);

//        btnEdit.setOnClickListener(v ->{
//            showToast("Edit Alamat");
//        });

    }


    //@OnClick(R.id.btn_edit)
//public void edit(View view){
//        showToast("Edit Alamat");
//
//}
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }
    private void setUp() {
        itemAlamat.add(new Alamat("Rumah","Rizky Rahmat Hakiki","Jl.Raya Cikoneng-Bojongsoang, Perumahan Cikoneng sakinah regency no 37",
                "Kab.Bandung","bojongsoang","Jawa Barat","40288","082216650210"));
    }
    public void edit(View view) {
        loadFragment(new FormEditAlamatFragment());
    }
    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_edit_alamat, fragment)
                    .commit();
            return true;
        }

        return false;
    }

    public void add(View view) {
        loadFragment(new FormAddAlamatFragment());
    }
}
