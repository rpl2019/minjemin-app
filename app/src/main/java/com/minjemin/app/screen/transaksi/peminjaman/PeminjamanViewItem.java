package com.minjemin.app.screen.transaksi.peminjaman;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.minjemin.app.R;
import com.minjemin.app.base.ViewItem;
import com.minjemin.app.models.Order;
import com.minjemin.app.screen.feedback.FeedbackActivity;
import com.minjemin.app.screen.pengembalian.PengembalianBarangActivity;
import com.minjemin.app.screen.transaksi.peminjaman.detail.DetailPeminjamanActivity;
import com.minjemin.app.utils.ViewTypeGenerator;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PeminjamanViewItem extends ViewItem<PeminjamanViewItem.Holder> {

    private Order mItem;

    public PeminjamanViewItem(Order mItem) {
        this.mItem = mItem;
    }

    @Override
    public int getViewType() {
        return new ViewTypeGenerator(getClass().getName()).getHasCode();
    }

    @Override
    public Holder createHolder(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new Holder(inflater.inflate(R.layout.item_barang_peminjaman, parent, false));
    }

    @Override
    public void bindHolder(Holder holder) {
        holder.bind(mItem);
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.image_item)
        ImageView image;
        @BindView(R.id.text_item_name)
        TextView name;
        @BindView(R.id.text_vendor)
        TextView vendor;
        @BindView(R.id.text_sisa_hari)
        TextView daysLeft;
        @BindView(R.id.text_status)
        TextView status;
        @BindView(R.id.button_kembalikan)
        Button btnReturn;
        @BindView(R.id.label_sisa_hari)
        TextView labelDaysLeft;
        private String id;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        void bind(Order item) {
            id = item.getId();
            Glide.with(itemView).load(item.getItem().getImages().get(0).getUrl()).into(image);
            name.setText(item.getItem().getName());
            vendor.setText(item.getItem().getVendor());
            switch (item.getStatus().get(0).getName().toUpperCase()) {
                case "UNPAID":
                case "PAID": {
                    status.setText("PENDING");
                    btnReturn.setVisibility(View.GONE);
                    daysLeft.setVisibility(View.GONE);
                    labelDaysLeft.setVisibility(View.GONE);
                    break;
                }
                case "PROCESS": {
                    status.setText("ONGOING");
                    status.setTextColor(itemView.getResources().getColor(R.color.colorPrimaryDark));
                    daysLeft.setText(item.getRemaining());
                    btnReturn.setOnClickListener(v -> {
                        Intent intent = new Intent(itemView.getContext(), PengembalianBarangActivity.class);
                        intent.putExtra("orderId", item.getId());
                        itemView.getContext().startActivity(intent);
                    });
                    break;
                }
                case "RETURNING": {
                    status.setText("RETURNED");
                    btnReturn.setVisibility(View.GONE);
                    daysLeft.setVisibility(View.GONE);
                    labelDaysLeft.setVisibility(View.GONE);
                    break;
                }
                default: {
                    status.setText("COMPLETED");
                    status.setTextColor(itemView.getResources().getColor(R.color.colorLightGreen));
                    btnReturn.setVisibility(View.GONE);
                    daysLeft.setVisibility(View.GONE);
                    labelDaysLeft.setVisibility(View.GONE);
                }
            }

        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), DetailPeminjamanActivity.class);
            intent.putExtra("id", id);
            v.getContext().startActivity(intent);
        }
    }
}
