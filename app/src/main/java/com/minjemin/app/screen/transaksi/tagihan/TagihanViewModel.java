package com.minjemin.app.screen.transaksi.tagihan;

import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.models.Image;
import com.minjemin.app.models.Invoice;
import com.minjemin.app.models.Item;
import com.minjemin.app.models.ItemTransaction;
import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.utils.StatedViewModel;
import com.minjemin.app.utils.UtilsApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TagihanViewModel extends StatedViewModel<TagihanViewModel.State> {

    private BaseApiService mApiService = UtilsApi.getApiService();

    @Override
    protected State initState() {
        return new State();
    }

    class State {
        boolean isLoading = false;
        List<Invoice> invoices = new ArrayList<>();
        Status status = Status.INIT;
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

//    void setupTagihan() {
//        List<Image> images = new ArrayList<>();
//        Image image = new Image();
//        image.setUrl("https://minjemin.com/uploads/item/5c89106f1593a_IMG_20190313_1320461813471074.jpg");
//        images.add(image);
//        image = new Image();
//        image.setUrl("https://minjemin.com/uploads/item/5c8910726938c_IMG_20190313_132139-1090489142.jpg");
//        images.add(image);
//        Item item = new Item();
//        item.setName("Mouse Logitech Gaming");
//        item.setImages(images);
//        item.setVendor("Alif Jafar");
//        ItemTransaction itemTr = new ItemTransaction();
//        itemTr.setItem(item);
////        itemTr.setRemain("1 Hari");
//        itemTr.setStatus("Belum Dibayar");
//        state.tagihans.add(itemTr);
//        updateState();
//    }

    void doGetInvoice() {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.invoiceMe()
                .enqueue(new Callback<BaseResponse<List<Invoice>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Invoice>>> call, Response<BaseResponse<List<Invoice>>> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.status = Status.SUCCESS;
                            state.invoices = response.body().getData();
                        } else {
                            state.status = Status.ERROR;
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<List<Invoice>>> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        updateState();
                    }
                });
    }
}
