package com.minjemin.app.screen.main.profile.notification;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.minjemin.app.R;
import com.minjemin.app.models.Notification;

import java.util.ArrayList;

public class NotificationActivity extends AppCompatActivity {
    @BindView(R.id.rv_notification)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar_notification)
    Toolbar toolbar;
    private RecyclerView.LayoutManager layoutManager;
    private NotificationAdapter adapter;
    private ArrayList<Notification> menu;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(getString(R.string.notifikasi));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mContext = recyclerView.getContext();
        menu = new ArrayList<>();

        setUp();
        adapter = new NotificationAdapter(mContext,menu);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(adapter);
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    private void setUp() {
        menu.add(new Notification("Barang anda telah sampai!","Selamat barang pinjaman anda telah sampai!","10 Maret 2019","09.32"+" WIB", v -> {
            showToast("Barang anda telah sampai!");
        }));
        menu.add(new Notification("Barang anda telah sampai!","Selamat barang pinjaman anda telah sampai!","10 Maret 2019","09.32"+" WIB", v -> {
            showToast("Barang anda telah sampai!");
        }));
        menu.add(new Notification("Barang anda telah sampai!","Selamat barang pinjaman anda telah sampai!","10 Maret 2019","09.32"+" WIB", v -> {
            showToast("Barang anda telah sampai!");
        }));
        menu.add(new Notification("Barang anda telah sampai!","Selamat barang pinjaman anda telah sampai!","10 Maret 2019","09.32"+" WIB", v -> {
            showToast("Barang anda telah sampai!");
        }));
    }
}
