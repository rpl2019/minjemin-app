package com.minjemin.app.screen.transaksi.peminjaman;

import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.models.Image;
import com.minjemin.app.models.Invoice;
import com.minjemin.app.models.Item;
import com.minjemin.app.models.ItemTransaction;
import com.minjemin.app.models.Order;
import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.screen.transaksi.tagihan.TagihanViewModel;
import com.minjemin.app.utils.StatedViewModel;
import com.minjemin.app.utils.UtilsApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PeminjamanViewModel extends StatedViewModel<PeminjamanViewModel.State> {

    private BaseApiService mApiService = UtilsApi.getApiService();

    @Override
    protected State initState() {
        return new State();
    }

    class State {
        boolean isLoading = false;
        List<Order> orders = new ArrayList<>();
        Status status = Status.INIT;
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

//    void setupPeminjaman() {
//        List<Image> images = new ArrayList<>();
//        Image image = new Image();
//        image.setUrl("https://minjemin.com/uploads/item/5c89106f1593a_IMG_20190313_1320461813471074.jpg");
//        images.add(image);
//        image = new Image();
//        image.setUrl("https://minjemin.com/uploads/item/5c8910726938c_IMG_20190313_132139-1090489142.jpg");
//        images.add(image);
//        Item item = new Item();
//        item.setName("Mouse Logitech Gaming");
//        item.setImages(images);
//        item.setVendor("Alif Jafar");
//        ItemTransaction itemTr = new ItemTransaction();
//        itemTr.setItem(item);
//        itemTr.setRemain("1 Hari");
//        itemTr.setStatus("Sedang Dipinjam");
//        state.peminjamans.add(itemTr);
//        itemTr = new ItemTransaction();
//        itemTr.setItem(item);
//        itemTr.setRemain("0 Hari");
//        itemTr.setStatus("Selesai");
//        state.peminjamans.add(itemTr);
//        updateState();
//    }

    void doGetOrders() {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.orderMe()
                .enqueue(new Callback<BaseResponse<List<Order>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Order>>> call, Response<BaseResponse<List<Order>>> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.status = Status.SUCCESS;
                            state.orders = response.body().getData();
                        } else {
                            state.status = Status.ERROR;
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<List<Order>>> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        updateState();
                    }
                });
    }
}
