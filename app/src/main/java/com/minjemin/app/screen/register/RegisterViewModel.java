package com.minjemin.app.screen.register;

import android.util.Log;

import com.minjemin.app.base.BaseErrorResponse;
import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.models.Register;
import com.minjemin.app.models.User;
import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.utils.ErrorUtils;
import com.minjemin.app.utils.StatedViewModel;
import com.minjemin.app.utils.UtilsApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterViewModel extends StatedViewModel<RegisterViewModel.State> {

    BaseApiService mApiService = UtilsApi.getApiService();

    @Override
    protected State initState() {
        return new State();
    }

    enum RegisterStatus {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        RegisterStatus status = RegisterStatus.INIT;
        User.ErrorRegister errors = new User().initErrorRegister();
    }

    void doRegister(String name, String username, String email, String password, String cPassword, String gender) {
        state.isLoading = true;
        state.status = RegisterStatus.INIT;
        updateState();
        mApiService.registerRequest(name, username, email, password, cPassword, gender)
                .enqueue(new Callback<BaseResponse<Register>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<Register>> call, Response<BaseResponse<Register>> response) {
                        state.isLoading = false;
                        state.errors.clear();
                        updateState();
                        if (response.isSuccessful()) {
                            state.status = RegisterStatus.SUCCESS;
                        } else {
                            state.status = RegisterStatus.ERROR;
                            BaseErrorResponse error = ErrorUtils.parseError(response);
                            if (error.getErrors().has("name"))
                                state.errors.setName(error.getErrors().get("name").getAsString());
                            if (error.getErrors().has("username"))
                                state.errors.setUsername(error.getErrors().get("username").getAsString());
                            if (error.getErrors().has("email"))
                                state.errors.setEmail(error.getErrors().get("email").getAsString());
                            if (error.getErrors().has("password"))
                                state.errors.setPassword(error.getErrors().get("password").getAsString());
                            if (error.getErrors().has("confirmation_password"))
                                state.errors.setConfirmationPassword(error.getErrors().get("confirmation_password").getAsString());
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<Register>> call, Throwable t) {
                        state.status = RegisterStatus.ERROR;
                        updateState();
                    }
                });
    }
}
