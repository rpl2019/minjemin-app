package com.minjemin.app.screen.vendor.item.extra;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.minjemin.app.R;
import com.minjemin.app.base.BaseRecycleAdapter;
import com.minjemin.app.models.Image;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageBarangAdapter extends BaseRecycleAdapter<Image> {

    private ItemListener mListener;
    private final static int TYPE = 2;

    public ImageBarangAdapter(Context context, ArrayList<Image> items, ItemListener listener) {
        super(context, items);
        this.mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder setViewHolder(ViewGroup parent, @Nullable int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_barang, null);
        return new Holder(view);
    }

    @Override
    public void onBindData(RecyclerView.ViewHolder holder, Image val) {
        Holder viewHodler = (Holder) holder;
        viewHodler.bindTo(val);
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.image_barang)
        ImageView itemImage;
        String filename;
        String id;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        void bindTo(Image val) {
            Glide.with(itemView).load(val.getUrl()).fitCenter().into(itemImage);
            filename = val.getFilename();
            id = val.getId();
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onItemClick(id, getAdapterPosition());
            }
        }
    }

    public interface ItemListener {
        void onItemClick(String id, int posititon);
    }
}
