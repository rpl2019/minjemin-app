package com.minjemin.app.screen.transaksi.tagihan;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.minjemin.app.R;
import com.minjemin.app.base.ViewItem;
import com.minjemin.app.models.Item;
import com.minjemin.app.models.Invoice;
import com.minjemin.app.screen.feedback.FeedbackActivity;
import com.minjemin.app.screen.transaksi.peminjaman.detail.DetailTransaksiActivity;
import com.minjemin.app.utils.ViewTypeGenerator;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TagihanViewItem extends ViewItem<TagihanViewItem.Holder> {

    private Invoice mItem;

    public TagihanViewItem(Invoice mItem) {
        this.mItem = mItem;
    }

    @Override
    public int getViewType() {
        return new ViewTypeGenerator(getClass().getName()).getHasCode();
    }

    @Override
    public Holder createHolder(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new Holder(inflater.inflate(R.layout.item_barang_tagihan, parent, false));
    }

    @Override
    public void bindHolder(Holder holder) {
        holder.bind(mItem);
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.image_item)
        ImageView image;
        @BindView(R.id.text_item_name)
        TextView name;
        @BindView(R.id.label_status)
        TextView status;
        @BindView(R.id.label_harga)
        TextView price;
        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        void bind(Invoice item) {
            Glide.with(itemView).load(item.getItem().getImages().get(0).getUrl()).fitCenter().into(image);
            name.setText(item.getItem().getName());
            status.setText(item.getStatus().get(0).getName().toUpperCase());
            if (item.getStatus().get(0).getName().equals("unpaid")) {
                status.setTextColor(itemView.getResources().getColor(R.color.colorLightOrange));
            } else {
                status.setTextColor(itemView.getResources().getColor(R.color.colorPrimaryDark));
            }
            price.setText("Rp. " + item.getAmountFormat());
        }

        @Override
        public void onClick(View v) {
            v.getContext().startActivity(new Intent(v.getContext(), DetailTransaksiActivity.class));
        }
    }
}
