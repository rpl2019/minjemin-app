package com.minjemin.app.screen.main.profile.edit_profile;

import android.util.Log;

import com.minjemin.app.base.BaseErrorResponse;
import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.data.AppState;
import com.minjemin.app.models.User;
import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.utils.ErrorUtils;
import com.minjemin.app.utils.StatedViewModel;
import com.minjemin.app.utils.UtilsApi;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileViewModel extends StatedViewModel<EditProfileViewModel.State> {

    private BaseApiService mApiService = UtilsApi.getApiService();
    private AppState appState = AppState.getInstance();

    @Override
    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        Status status = Status.INIT;
        User currentUser = AppState.getInstance().getUser();
        String message = "";
    }

    void getUser() {
        state.currentUser = AppState.getInstance().getUser();
    }


    void doUpdateProfile(String id, String name, String email, String gender, String phone) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.updateProfile(id, name, email, gender, phone)
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        state.isLoading = false;

                        if (response.isSuccessful()) {
                            state.status = Status.SUCCESS;
                            state.message = response.body().getMessage();
                            doGetUser();
                        } else {
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.status = Status.ERROR;
                            state.message = errorResponse.getMessage();
                        }

                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        state.message = t.getMessage();
                    }
                });
    }

    void doGetUser() {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.getCurrentUser()
                .enqueue(new Callback<BaseResponse<User>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<User>> call, Response<BaseResponse<User>> response) {
                        state.isLoading = false;
                        appState.saveUser(response.body().getData());
                        Log.d("USER", "User Loaded");
                        getUser();
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<User>> call, Throwable t) {

                    }
                });
    }

    void doUpdateAvatar(MultipartBody.Part avatar) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.updateAvatar(avatar)
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.status = Status.SUCCESS;
                            state.message = response.body().getMessage();
                            doGetUser();
                        } else {
                            state.status = Status.ERROR;
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.message = errorResponse.getMessage();
                            Log.d("ERROR", errorResponse.getErrors().toString());
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        state.isLoading = false;
                        state.message = t.getMessage();
                        state.status = Status.ERROR;
                        updateState();
                    }
                });
    }
}
