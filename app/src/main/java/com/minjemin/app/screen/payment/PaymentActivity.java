package com.minjemin.app.screen.payment;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.minjemin.app.R;
import com.minjemin.app.screen.main.MainActivity;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class PaymentActivity extends AppCompatActivity {

    @BindView(R.id.text_order_number)
    EditText textorderNumber;
    @BindView(R.id.text_account_number)
    EditText textaccountNumber;
    @BindView(R.id.text_account_name)
    EditText textaccountName;
    @BindView(R.id.text_amount)
    EditText textAmount;
    @BindView(R.id.text_bank_name)
    EditText textBankName;
    @BindView(R.id.btn_image)
    Button btnImage;
    @BindView(R.id.btn_bayar)
    Button btnBayar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private PaymentViewModel viewModel;
    private String orderNumber;
    private String amount;
    MultipartBody.Part theImage;
    private Image tmpImage;
    private Context mContext;
    private ImagePicker imagePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);
        mContext = this;
        viewModel = ViewModelProviders.of(this).get(PaymentViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderPayment(state.status, state.message);
        });
        init();
    }

    void init() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            orderNumber = getIntent().getStringExtra("number");
            amount = getIntent().getStringExtra("amount");
            textorderNumber.setText(orderNumber);
            textAmount.setText(amount);
        }

        btnBayar.setOnClickListener(v -> doUploadPayment());
        btnImage.setOnClickListener(v -> {
            getImagePicker().start();
        });

    }

    void renderLoading(boolean isLoading) {
        progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    void renderPayment(PaymentViewModel.Status status, String msg) {
        if (status == PaymentViewModel.Status.SUCCESS && !msg.equals("")) {
            Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(mContext, MainActivity.class);
            intent.putExtra("transaksi", true);
            startActivity(intent);
            finish();
        } else if (status == PaymentViewModel.Status.ERROR) {
            Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
        }
    }

    void doUploadPayment() {

        if (!validate())
            return;

        viewModel.doPayment(textorderNumber.getText().toString(), textBankName.getText().toString(),
                textaccountNumber.getText().toString(),
                textaccountName.getText().toString(), textAmount.getText().toString(), theImage);
    }

    private boolean validate() {
        boolean valid = true;
        if (textorderNumber.getText().toString().isEmpty() || textaccountNumber.getText().toString().isEmpty()
                || textaccountName.getText().toString().isEmpty() || textBankName.getText().toString().isEmpty() ||
                textAmount.getText().toString().isEmpty() || theImage == null) {
            valid = false;
            Toast.makeText(getApplicationContext(), "Isi Data Dengan Lengkap", Toast.LENGTH_SHORT).show();
        }


        return valid;
    }


    public ImagePicker getImagePicker() {
        imagePicker = ImagePicker.create(this)
                .theme(R.style.ImagePickerTheme)
                .toolbarImageTitle("Tap to select")
                .toolbarDoneButtonText("DONE")
                .folderMode(true)
                .single();

        return imagePicker.limit(1)
                .showCamera(true)
                .imageDirectory("camera")
                .imageFullDirectory(Environment.getExternalStorageDirectory().getPath());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            tmpImage = ImagePicker.getFirstImageOrNull(data);
            uploadImage(tmpImage);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void uploadImage(Image image) {
        if (image == null) return;

        File file = new File(image.getPath());
        File compresedImg = null;
        try {
            compresedImg = new Compressor(mContext).compressToFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), compresedImg);
        MultipartBody.Part img = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        Toast.makeText(mContext, "Image Selected", Toast.LENGTH_SHORT).show();
        theImage = img;
    }


}
