package com.minjemin.app.screen.transaksi.peminjaman.detail;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.minjemin.app.R;
import com.minjemin.app.models.Order;
import com.minjemin.app.screen.feedback.FeedbackActivity;
import com.minjemin.app.screen.item.ItemDetailViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailPeminjamanActivity extends AppCompatActivity {

    @BindView(R.id.tv_storeName)
    TextView storeName;
    @BindView(R.id.tv_item_name)
    TextView itemName;
    @BindView(R.id.tv_total_price)
    TextView itemTotalPrice;
    @BindView(R.id.image_item)
    ImageView itemImage;
    @BindView(R.id.tv_transaction_number)
    TextView transactionNumber;
    @BindView(R.id.btn_feedback)
    Button btnFeedback;
    private String orderId;
    private DetailPeminjamanViewModel viewModel;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_peminjaman);
        ButterKnife.bind(this);
        mContext = this;
        init();
    }

    void init() {
        orderId = getIntent().getStringExtra("id");
        viewModel = ViewModelProviders.of(this).get(DetailPeminjamanViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderItem(state.order, state.status);
        });
        viewModel.getOrderDetail(orderId);
    }

    void renderItem(Order order, DetailPeminjamanViewModel.Status status) {
        if (order != null && status == DetailPeminjamanViewModel.Status.SUCCESS) {
            storeName.setText(order.getItem().getVendor());
            itemName.setText(order.getItem().getName());
            itemTotalPrice.setText(order.getAmountFormat());
            Glide.with(this).load(order.getItem().getImages().get(0).getUrl()).centerCrop()
                    .into(itemImage);
            transactionNumber.setText(order.getNumber());
            if (order.getStatus().get(0).getName().equals("completed")) {
                btnFeedback.setVisibility(View.VISIBLE);
                btnFeedback.setOnClickListener(v -> {
                    Intent intent = new Intent(mContext, FeedbackActivity.class);
                    intent.putExtra("id", order.getItem().getId());
                    startActivity(intent);
                });
            }
        }
    }
}
