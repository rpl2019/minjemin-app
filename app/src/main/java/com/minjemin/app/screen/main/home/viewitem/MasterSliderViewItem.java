package com.minjemin.app.screen.main.home.viewitem;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.minjemin.app.R;
import com.minjemin.app.base.BaseRecyclerAdapter;
import com.minjemin.app.base.ViewItem;
import com.minjemin.app.models.Banner;
import com.minjemin.app.models.Home;
import com.minjemin.app.screen.main.home.ScrollingLinearLayoutManager;
import com.minjemin.app.utils.ViewTypeGenerator;

import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator2;

public class MasterSliderViewItem extends ViewItem<MasterSliderViewItem.Holder> {

    private Home<Banner> item;

    public MasterSliderViewItem(Home<Banner> item) {
        this.item = item;
    }

    @Override
    public int getViewType() {
        return new ViewTypeGenerator(getClass().getName()).getHasCode();
    }

    @Override
    public Holder createHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_banner_slider,
                null);
        return new Holder(view);
    }

    @Override
    public void bindHolder(Holder holder) {
        holder.bind(item);
    }

    class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.slider_sub_title)
        TextView subTitle;
        @BindView(R.id.slider_view_all)
        TextView viewAll;
        @BindView(R.id.slider_rv_item)
        RecyclerView recyclerView;
        @BindView(R.id.slider_indicators)
        CircleIndicator2 circleIndicator2;
        LinearSnapHelper linearSnapHelper;
        ScrollingLinearLayoutManager scrollingLinear;
        BaseRecyclerAdapter adapter;
        SwipeTask swipeTask;
        Timer swipeTimer;
        private boolean loading = true;
        int pastVisiblesItems, visibleItemCount, totalItemCount;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            adapter = new BaseRecyclerAdapter();
            scrollingLinear = new ScrollingLinearLayoutManager(itemView.getContext(),
                    LinearLayoutManager.HORIZONTAL, false, 100);
            recyclerView.setLayoutManager(scrollingLinear);
            linearSnapHelper = new LinearSnapHelper();
            recyclerView.setAdapter(adapter);
            linearSnapHelper.attachToRecyclerView(recyclerView);
            circleIndicator2.attachToRecyclerView(recyclerView, linearSnapHelper);

            adapter.clear();
        }

        void bind(Home<Banner> val) {
            subTitle.setText(val.getTitle());
            adapter.clearOnly();

            for (Banner bn : val.getItemList()) {
                adapter.addItems(new BannerViewItem(bn));
            }

            viewAll.setOnClickListener(v -> {
                Toast.makeText(itemView.getContext(), "You're Clicked " + val.getTitle()
                        + " ViewAll", Toast.LENGTH_SHORT).show();
            });

//            swipeTask = new SwipeTask();
//            swipeTimer = new Timer();
//            playCarousel();
//
//            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//                @Override
//                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                    if (dy > 0) //check for scroll down
//                    {
//                        visibleItemCount = scrollingLinear.getChildCount();
//                        totalItemCount = scrollingLinear.getItemCount();
//                        pastVisiblesItems = scrollingLinear.findFirstVisibleItemPosition();
//
//                        if (loading) {
//                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
//                                loading = false;
//                            }
//                        }
//                    }
//                }
//            });
            adapter.notifyDataSetChanged();
        }

        private class SwipeTask extends TimerTask {
            public void run() {
                recyclerView.post(() -> {
                    int nextPage = (scrollingLinear.findFirstVisibleItemPosition() + 1)
                            % adapter.getItemCount();
                    recyclerView.smoothScrollToPosition(nextPage);
                });
            }
        }

        private void stopScrollTimer() {
            if (null != swipeTimer) {
                swipeTimer.cancel();
            }
            if (null != swipeTask) {
                swipeTask.cancel();
            }
        }

        private void resetScrollTimer() {
            stopScrollTimer();
            swipeTask = new SwipeTask();
            swipeTimer = new Timer();
        }

        private void playCarousel() {
            resetScrollTimer();
            swipeTimer.schedule(swipeTask, 0, 4000);
        }
    }


}
