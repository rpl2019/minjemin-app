package com.minjemin.app.screen.settings;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import com.minjemin.app.R;
import com.minjemin.app.data.AppState;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingNotificationActivity extends AppCompatActivity {

    @BindView(R.id.switch_notification)
    Switch swNotification;
    @BindView(R.id.switch_sound)
    Switch swSound;
    @BindView(R.id.switch_vibrate)
    Switch swVibrate;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.toolbar_notification)
    Toolbar toolbar;

    private AppState appState = AppState.getInstance();
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_notification);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.setting_notification));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = this;
        init();
    }

    void init() {
        swNotification.setChecked(appState.isNotificaiton());
        swVibrate.setChecked(appState.isVibrate());
        swSound.setChecked(appState.isSound());
        btnSubmit.setOnClickListener(v -> {
            changeAppNoticiation();
            changeSound();
            changeVibrate();
            Toast.makeText(mContext, "Berhasil Mengubah Peraturan", Toast.LENGTH_SHORT).show();
        });
    }

    void changeAppNoticiation() {
        if (swNotification.isChecked()) {
            appState.setIsNotification(true);
        } else {
            appState.setIsNotification(false);
        }
    }

    void changeVibrate() {
        if (swVibrate.isChecked()) {
            appState.setIsVibrate(true);
        } else {
            appState.setIsVibrate(false);
        }
    }

    void changeSound() {
        if (swSound.isChecked()) {
            appState.setIsSound(true);
        } else {
            appState.setIsSound(false);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
