package com.minjemin.app.screen.main.home.viewitem;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.minjemin.app.R;
import com.minjemin.app.base.ViewItem;
import com.minjemin.app.models.Item;
import com.minjemin.app.screen.item.ItemDetailActivity;
import com.minjemin.app.utils.ViewTypeGenerator;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductViewItem extends ViewItem<ProductViewItem.Holder> {

    private Item mItem;

    public ProductViewItem(Item mItem) {
        this.mItem = mItem;
    }

    @Override
    public int getViewType() {
        return new ViewTypeGenerator(getClass().getName()).getHasCode();
    }

    @Override
    public Holder createHolder(ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_barang, parent, false);
        return new Holder(view);
    }

    @Override
    public void bindHolder(Holder holder) {
        holder.bind(mItem);
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.image_item)
        ImageView imageView;
        @BindView(R.id.text_item)
        TextView textItem;
        @BindView(R.id.text_price)
        TextView textPrice;
        @BindView(R.id.rating_bar)
        RatingBar ratingBar;
        private String id;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        private void bind(Item item) {
            Glide.with(itemView).load(item.getImages().get(0).getUrl()).centerCrop().into(imageView);
            textItem.setText(item.getName());
            textPrice.setText("Rp. " + item.getPriceFormat());
            id = item.getId();
            float rating = item.getRating().getAverageRate() != null ?
                    Float.parseFloat(item.getRating().getAverageRate()) : 0f;
            ratingBar.setRating(rating);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(itemView.getContext(), ItemDetailActivity.class);
            intent.putExtra("id", id);
            itemView.getContext().startActivity(intent);
        }
    }
}
