package com.minjemin.app.screen.feedback;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.minjemin.app.R;
import com.minjemin.app.models.Item;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedbackActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.image_item)
    ImageView itemImage;
    @BindView(R.id.tv_item_name)
    TextView itemName;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.text_title)
    EditText textTitle;
    @BindView(R.id.text_feedback)
    EditText textFeedback;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    private FeedbackViewModel viewModel;
    private Context mContext;
    private String itemId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Berikan Ulasanmu");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = this;
        itemId = getIntent().getStringExtra("id");
        viewModel = ViewModelProviders.of(this).get(FeedbackViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderStatus(state.status);
            renderItem(state.item);
        });
        viewModel.getItem(itemId);
        sendFeedback();
    }

    void sendFeedback() {
        btnSubmit.setOnClickListener(v -> {
            if (textTitle.getText().toString().isEmpty() && textFeedback.getText().toString().isEmpty()) {
                Toast.makeText(mContext, "Judul dan Body Feedback Wajib Diisi", Toast.LENGTH_LONG).show();
                return;
            }

            if (ratingBar.getRating() == 0) {
                Toast.makeText(mContext, "Rating Belum diisi", Toast.LENGTH_SHORT).show();
                return;
            }
            viewModel.doSendFeedback(itemId, String.valueOf(ratingBar.getRating()),
                    textFeedback.getText().toString());
        });
    }

    void renderLoading(boolean isLoading) {
        // TODO
    }

    void renderStatus(FeedbackViewModel.Status status) {
        if (status == FeedbackViewModel.Status.SUCCESS) {
            Toast.makeText(mContext, "Terimakasih sudah mengisi feedback", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    void renderItem(Item item) {
        if (item != null) {
            itemName.setText(item.getName());
            Glide.with(this).load(item.getImages().get(0).getUrl()).centerCrop()
                    .into(itemImage);
        }
    }
}
