package com.minjemin.app.screen.login;

import android.util.Log;

import com.minjemin.app.data.AppState;
import com.minjemin.app.base.BaseErrorResponse;
import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.models.Login;
import com.minjemin.app.models.User;
import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.utils.ErrorUtils;
import com.minjemin.app.utils.StatedViewModel;
import com.minjemin.app.utils.UtilsApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends StatedViewModel<LoginViewModel.State> {

    private static final String TAG = "LoginActivity";

    BaseApiService mApiService = UtilsApi.getApiService();

    AppState appState = AppState.getInstance();

    @Override
    protected State initState() {
        return new State();
    }

    enum LoginStatus {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        LoginStatus status = LoginStatus.INIT;
        String message = null;
    }


    void doLogin(String email, String password) {
        state.isLoading = true;
        state.status = LoginStatus.INIT;
        updateState();
        mApiService.loginRequest(email, password).enqueue(new Callback<BaseResponse<Login>>() {
            @Override
            public void onResponse(Call<BaseResponse<Login>> call, Response<BaseResponse<Login>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = LoginStatus.SUCCESS;
                    state.message = response.body().getMessage();
                    appState.setToken(response.body().getData().getToken());
                    appState.setIsLoggedIn(true);
                    Log.d(TAG, "User Login Success");
                    getUser();

                } else {
                    BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                    state.status = LoginStatus.ERROR;
                    state.message = errorResponse.getMessage();
                }
                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<Login>> call, Throwable t) {
                state.status = LoginStatus.ERROR;
                updateState();
            }
        });
    }

    public void getUser() {
        mApiService.getCurrentUser()
                .enqueue(new Callback<BaseResponse<User>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<User>> call, Response<BaseResponse<User>> response) {
                        appState.saveUser(response.body().getData());
                        Log.d(TAG, "User Loaded");
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<User>> call, Throwable t) {

                    }
                });
    }

}
