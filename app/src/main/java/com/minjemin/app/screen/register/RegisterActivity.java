package com.minjemin.app.screen.register;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.minjemin.app.R;
import com.minjemin.app.models.User;


public class RegisterActivity extends AppCompatActivity {

    RegisterViewModel viewModel;

    @BindView(R.id.textName)
    EditText textName;
    @BindView(R.id.textUsername)
    EditText textUsername;
    @BindView(R.id.textEmail)
    EditText textEmail;
    @BindView(R.id.textPassword)
    EditText textPassword;
    @BindView(R.id.textConfirmationPassword)
    EditText textCoPassword;
    @BindView(R.id.radioMale)
    RadioButton radioMale;
    @BindView(R.id.buttonRegister)
    Button buttonRegister;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.tvLogin)
    TextView tvLogin;

    @BindView(R.id.input_layout_name)
    TextInputLayout layoutName;
    @BindView(R.id.input_layout_username)
    TextInputLayout layoutUsername;
    @BindView(R.id.input_layout_email)
    TextInputLayout layoutEmail;
    @BindView(R.id.input_layout_password)
    TextInputLayout layoutPassword;
    @BindView(R.id.input_layout_confirm_pass)
    TextInputLayout layoutConfirmPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this).get(RegisterViewModel.class);

        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderRegisterResponse(state.status);
            renderErrors(state.errors);
        });

        iniListener();
    }

    void iniListener() {

        String gender = radioMale.isSelected() ? "L" : "P";
        buttonRegister.setOnClickListener(view -> {
            viewModel.doRegister(textName.getText().toString(),
                    textUsername.getText().toString(),
                    textEmail.getText().toString(),
                    textPassword.getText().toString(),
                    textCoPassword.getText().toString(),
                    gender);
        });

        tvLogin.setOnClickListener(view -> {
            finish();
        });
        changeListenerLayoutText();
    }

    void renderLoading(Boolean isLoading) {
        buttonRegister.setEnabled(!isLoading);
        progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    void renderRegisterResponse(RegisterViewModel.RegisterStatus status) {
        if (status == RegisterViewModel.RegisterStatus.SUCCESS) {
            showToast("Register Success");
        } else if (status == RegisterViewModel.RegisterStatus.ERROR) {
            showToast("Register Failed");
        }
    }

    void renderErrors(User.ErrorRegister error) {
        if (error.getName() != null)
            layoutName.setError(error.getName());
        else layoutName.setError(null);
        if (error.getUsername() != null)
            layoutUsername.setError(error.getUsername());
        else layoutName.setError(null);
        if (error.getEmail() != null)
            layoutEmail.setError(error.getEmail());
        else layoutEmail.setError(null);
        if (error.getPassword() != null)
            layoutPassword.setError(error.getPassword());
        else layoutPassword.setError(null);
        if (error.getConfirmationPassword() != null)
            layoutConfirmPass.setError(error.getConfirmationPassword());
        else layoutConfirmPass.setError(null);
    }

    void showToast(String m) {
        Toast.makeText(this, m, Toast.LENGTH_SHORT).show();
    }

    private void changeListenerLayoutText() {
        textName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                layoutName.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        textUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                layoutUsername.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        textEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                layoutEmail.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        textPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                layoutPassword.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        textCoPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                layoutConfirmPass.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
