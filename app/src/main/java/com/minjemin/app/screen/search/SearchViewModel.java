package com.minjemin.app.screen.search;

import android.util.Log;

import com.minjemin.app.base.BaseErrorResponse;
import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.models.Item;
import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.utils.ErrorUtils;
import com.minjemin.app.utils.StatedViewModel;
import com.minjemin.app.utils.UtilsApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchViewModel extends StatedViewModel<SearchViewModel.State> {

    private BaseApiService mApiService = UtilsApi.getApiService();

    @Override
    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR;
    }

    class State {
        Status status = Status.INIT;
        boolean isLoading = false;
        List<Item> items = new ArrayList<>();
    }

    public void doSearch(String keyword) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.requestSearch(keyword)
                .enqueue(new Callback<BaseResponse<List<Item>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Item>>> call, Response<BaseResponse<List<Item>>> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.items = response.body().getData();
                            state.status = Status.SUCCESS;
                        } else {
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            Log.d("SearchAction", errorResponse.getMessage());
                            state.status = Status.ERROR;
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<List<Item>>> call, Throwable t) {
                        state.isLoading = false;
                        Log.d("SearchAction", t.getMessage());
                        state.status = Status.ERROR;
                        updateState();
                    }
                });
    }
}
