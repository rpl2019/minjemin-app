package com.minjemin.app.screen.vendor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.minjemin.app.R;
import com.minjemin.app.screen.vendor.item.ItemFragment;
import com.minjemin.app.screen.vendor.item.create.VendorCreateActivity;

public class VendorActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_vendor)
    Toolbar toolbar;
    @BindView(R.id.pager_vendor)
    ViewPager viewPager;
    @BindView(R.id.tabs_vendor)
    TabLayout tabs;
    @BindView(R.id.fab_add_barang)
    FloatingActionButton fabAddBarang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.vendor_activity));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager.setAdapter(new VendorPagerAdapter(getSupportFragmentManager(), 2));
        tabs.setupWithViewPager(viewPager);

        fabAddBarang.setOnClickListener(v -> {
            startActivity(new Intent(this, VendorCreateActivity.class));
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    class VendorPagerAdapter extends FragmentStatePagerAdapter {
        int mNumTabs;

        public VendorPagerAdapter(@NonNull FragmentManager fm, int mNumTabs) {
            super(fm);
            this.mNumTabs = mNumTabs;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.status_published);
                case 1:
                    return getString(R.string.status_draft);
                default:
                    return null;

            }
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return ItemFragment.newInstance(1);
                case 1:
                    return ItemFragment.newInstance(2);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumTabs;
        }
    }
}
