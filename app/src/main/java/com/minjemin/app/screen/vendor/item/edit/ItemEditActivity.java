package com.minjemin.app.screen.vendor.item.edit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.minjemin.app.R;
import com.minjemin.app.models.Category;
import com.minjemin.app.models.Item;
import com.minjemin.app.screen.vendor.item.create.VendorCreateActivity;
import com.minjemin.app.screen.vendor.item.create.VendorCreateViewModel;
import com.minjemin.app.screen.vendor.item.extra.CategoryAdapter;
import com.minjemin.app.screen.vendor.item.extra.ImageBarangAdapter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ItemEditActivity extends AppCompatActivity implements CategoryAdapter.ItemListener, ImageBarangAdapter.ItemListener {

    @BindView(R.id.toolbar_add_barang)
    Toolbar toolbar;
    @BindView(R.id.bottom_sheet_category)
    View bottomSheet;
    @BindView(R.id.rv_categories)
    RecyclerView recyclerView;
    @BindView(R.id.choosen_category)
    TextView chooseCategory;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.frame_foto_barang)
    FrameLayout layoutFoto;
    @BindView(R.id.rv_image_barang)
    RecyclerView rvImageBarang;
    @BindView(R.id.recycler_empty)
    TextView emptyImage;
    @BindView(R.id.text_nama)
    EditText itemName;
    @BindView(R.id.text_description)
    EditText itemDesc;
    @BindView(R.id.text_price)
    EditText itemPrice;
    @BindView(R.id.button_draft)
    Button btnDraft;
    @BindView(R.id.button_publish)
    Button btnPublish;
    private Context mContext;
    private ItemEditViewModel viewModel;
    private CategoryAdapter categoryAdapter;
    private BottomSheetBehavior bottomSheetBehavior;
    private Image tmpImage;
    private ImagePicker imagePicker;
    private ImageBarangAdapter imageBarangAdapter;
    private String category_id;
    private String[] image_id;
    private String item_id;

    private enum Status {
        published, draft;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_item_edit);
        ButterKnife.bind(this);
        mContext = this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.vendor_edit_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    private void init() {
        item_id = getIntent().getStringExtra("item_id");
        viewModel = ViewModelProviders.of(this).get(ItemEditViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoaded(state.item);
            renderLoading(state.isLoading);
            renderCategories(state.categories, state.status);
            renderImage(state.listImage, state.status, state.message);
            renderIsUpdated(state.isUpdated);
        });
        viewModel.doGetCategories();
        setupBottomSheet();
        viewModel.doGetItemMe(item_id);
        chooseCategory.setOnClickListener(v -> {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);

        });

        showRecylcerImage();

        layoutFoto.setOnClickListener(v -> {
            if (viewModel.getState().getValue().listImage != null && viewModel.getState().getValue().listImage.size() >= 5) {
                Toast.makeText(mContext, "Maksimal 5 Foto", Toast.LENGTH_SHORT).show();
                return;
            }
            getImagePicker().start();
        });

        btnPublish.setOnClickListener(v -> {

            if (!validated())
                return;

            image_id = new String[viewModel.getState().getValue().listImage.size()];
            for (int i = 0; i < image_id.length; i++) {
                image_id[i] = viewModel.getState().getValue().listImage.get(i).getId();
            }

            viewModel.doUpdateItem(item_id, itemName.getText().toString(), itemDesc.getText().toString(), itemPrice.getText().toString(), category_id, Status.published.toString(), image_id);
        });

        btnDraft.setOnClickListener(v -> {
            if (!validated())
                return;

            image_id = new String[viewModel.getState().getValue().listImage.size()];
            for (int i = 0; i < image_id.length; i++) {
                image_id[i] = viewModel.getState().getValue().listImage.get(i).getId();
            }

            viewModel.doUpdateItem(item_id, itemName.getText().toString(), itemDesc.getText().toString(), itemPrice.getText().toString(), category_id, Status.draft.toString(), image_id);
        });
    }

    void renderLoaded(Item item) {
        if (item != null) {
            itemName.setText(item.getName());
            itemDesc.setText(item.getDescription());
            itemPrice.setText(item.getPrice());
            category_id = item.getCategoryId();
            chooseCategory.setText(item.getCategory());
        }
    }

    void showRecylcerImage() {
        if (viewModel.getState().getValue().listImage == null || viewModel.getState().getValue().listImage.isEmpty()) {
            rvImageBarang.setVisibility(View.GONE);
            emptyImage.setVisibility(View.VISIBLE);
            emptyImage.setOnClickListener(v -> {
                getImagePicker().start();
            });
        } else {
            rvImageBarang.setVisibility(View.VISIBLE);
            emptyImage.setVisibility(View.GONE);
        }

    }

    public ImagePicker getImagePicker() {
        imagePicker = ImagePicker.create(this)
                .theme(R.style.ImagePickerTheme)
                .toolbarImageTitle("Tap to select")
                .toolbarDoneButtonText("DONE")
                .single();

        return imagePicker.limit(5)
                .showCamera(true)
                .imageDirectory("camera")
                .imageFullDirectory(Environment.getExternalStorageDirectory().getPath());
    }

    void renderCategories(ArrayList<Category> categories, ItemEditViewModel.Status status) {
        if (status == ItemEditViewModel.Status.SUCCESS) {
            categoryAdapter = new CategoryAdapter(mContext, categories, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            recyclerView.setAdapter(categoryAdapter);
        } else if (status == ItemEditViewModel.Status.ERROR) {
            viewModel.doGetCategories();
        }
    }

    private void setupBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {

            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
    }

    void renderLoading(Boolean isLoading) {
        progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            tmpImage = ImagePicker.getFirstImageOrNull(data);
            uploadImage(tmpImage);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void uploadImage(Image images) {
        if (images == null) return;

        File file = new File(images.getPath());
        File compresedImg = null;
        try {
            compresedImg = new Compressor(mContext).compressToFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), compresedImg);
        MultipartBody.Part img = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        viewModel.doUploadImage(img);
    }

    private void renderImage(ArrayList<com.minjemin.app.models.Image> images, ItemEditViewModel.Status status, String message) {
        if (status == ItemEditViewModel.Status.SUCCESS && images != null) {
            imageBarangAdapter = new ImageBarangAdapter(mContext, images, this);
            rvImageBarang.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            rvImageBarang.setAdapter(imageBarangAdapter);

            if (message != null)
                Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();

            if (!images.isEmpty())
                showRecylcerImage();
        } else if (status == ItemEditViewModel.Status.ERROR) {
            if (message != null)
                Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onItemClick(String name, String id) {
        chooseCategory.setText(name);
        this.category_id = id;
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    @Override
    public void onItemClick(String id, int posititon) {
        PopupMenu popupMenu = new PopupMenu(rvImageBarang.getContext(), rvImageBarang);
        popupMenu.inflate(R.menu.image_menu);
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.itemDelete:
                    viewModel.doDeleteImage(id, posititon);
                    return true;
            }
            return false;
        });

        popupMenu.show();
    }

    private boolean validated() {
        boolean isValid = true;
        String msg = "Field is Required";

        if (itemName.getText().toString().isEmpty()) {
            itemName.setError(msg);
            isValid = false;
        } else if (itemDesc.getText().toString().isEmpty()) {
            itemDesc.setError(msg);
            isValid = false;
        } else if (chooseCategory.getText().toString().isEmpty()) {
            isValid = false;
        } else if (itemPrice.getText().toString().isEmpty()) {
            itemPrice.setError(msg);
            isValid = false;
        } else if (viewModel.getState().getValue().listImage == null || viewModel.getState().getValue().listImage.isEmpty()) {
            isValid = false;
        } else if (category_id.isEmpty()) {
            isValid = false;
        }

        return isValid;
    }

    void renderIsUpdated(boolean isUpdated) {
        if (isUpdated)
            finish();
    }
}
