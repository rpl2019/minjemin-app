package com.minjemin.app.screen.transaksi.tagihan;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.minjemin.app.R;
import com.minjemin.app.base.BaseRecyclerAdapter;
import com.minjemin.app.models.Invoice;
import com.minjemin.app.models.ItemTransaction;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TagihanFragment extends Fragment {

    @BindView(R.id.rv_vendor_item)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    private TagihanViewModel viewModel;
    private BaseRecyclerAdapter adapter;
    private Context mContext;

    public static TagihanFragment instance() {
        return new TagihanFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.vendor_fragment_item_barang, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        adapter = new BaseRecyclerAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewModel = ViewModelProviders.of(this).get(TagihanViewModel.class);
        viewModel.getState().observe(this, state -> {
            adapter.clearOnly();
            renderItemTransaksi(state.invoices);
            renderLoading(state.isLoading);
            adapter.notifyDataSetChanged();
        });
        init();

        swipeRefreshLayout.setOnRefreshListener(this::init);
    }

    void init() {
        viewModel.doGetInvoice();
    }

    void renderItemTransaksi(List<Invoice> items) {
        if (!items.isEmpty()) {
            for (Invoice inv : items) {
                adapter.addItems(new TagihanViewItem(inv));
            }
        }
    }


    private void renderLoading(boolean isLoading) {
        if (isLoading) {
            swipeRefreshLayout.setRefreshing(true);
        } else {
            swipeRefreshLayout.setRefreshing(false);
        }
    }
}
