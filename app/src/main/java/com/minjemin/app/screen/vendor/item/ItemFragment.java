package com.minjemin.app.screen.vendor.item;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.minjemin.app.R;
import com.minjemin.app.base.BaseRecycleAdapter;
import com.minjemin.app.base.BaseRecyclerAdapter;
import com.minjemin.app.models.Item;
import com.minjemin.app.models.StateEmpty;
import com.minjemin.app.screen.extras.EmptyItem;
import com.minjemin.app.screen.vendor.VendorActivity;
import com.minjemin.app.screen.vendor.item.edit.ItemEditActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemFragment extends Fragment implements PublishedViewItem.ItemListener, DraftViewItem.ItemListener {

    @BindView(R.id.rv_vendor_item)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    private Context mContext;
    private final static int TYPE_1 = 1;
    private Integer type;
    private BaseRecyclerAdapter adapter;
    private BaseRecyclerAdapter adapterDraft;
    private AlertDialog.Builder deleteDialog;
    ItemViewModel mViewModel;

    private ArrayList<Item> itemPublished;
    private ArrayList<Item> itemDraft;

    public static ItemFragment newInstance(Integer type) {
        ItemFragment itemFragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putInt("TYPE", type);
        itemFragment.setArguments(args);
        return itemFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.vendor_fragment_item_barang, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mContext = getContext();
        itemDraft = new ArrayList<>();
        itemPublished = new ArrayList<>();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ItemViewModel.class);
        type = getArguments().getInt("TYPE");
        init();
    }

    protected void init() {
        mViewModel.getState().observe(this, state -> {
            if (type == TYPE_1) {
                renderPublished(state.publishedItems, state.status);
            } else {
                renderDraft(state.draftItems, state.status);
            }
            renderLoading(state.isLoading);
            renderMessage(state.message);
        });

        if (type == TYPE_1) {
            mViewModel.getPublishedItem();
        } else {
            mViewModel.getDraftItem();
        }

        swipeRefreshLayout.setOnRefreshListener(() -> {
            if (type == TYPE_1) {
                mViewModel.getPublishedItem();
            } else {
                mViewModel.getDraftItem();
            }
        });
        buildDialog();
    }

    private void buildDialog() {
        deleteDialog = new AlertDialog.Builder(mContext);
        deleteDialog.setTitle(getString(R.string.title_alert_delete_item));
        deleteDialog.setMessage(getString(R.string.msg_alert_delete_item));
    }

    private void renderPublished(List<Item> items, ItemViewModel.Status status) {
        adapter = new BaseRecyclerAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        if (items.isEmpty()) {
            adapter.addItems(new EmptyItem(new StateEmpty(getString(R.string.empty_item_published_title), getString(R.string.empty_item_published_desc))));
        } else if (status == ItemViewModel.Status.SUCCESS && !items.isEmpty()) {
            adapter.clear();
            for (Item item : items) {
                adapter.addItems(new PublishedViewItem(item, this));
            }
        } else if (status == ItemViewModel.Status.ERROR && items.isEmpty()) {
            mViewModel.getPublishedItem();
        }
    }

    private void renderDraft(List<Item> items, ItemViewModel.Status status) {
        adapter = new BaseRecyclerAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        if (items.isEmpty()) {
            adapter.addItems(new EmptyItem(new StateEmpty(getString(R.string.empty_item_draft_title), getString(R.string.empty_item_draft_desc))));
        } else if (status == ItemViewModel.Status.SUCCESS && !items.isEmpty()) {
            adapter.clear();
            for (Item item : items) {
                adapter.addItems(new DraftViewItem(item, this));
            }
        } else if (status == ItemViewModel.Status.ERROR && items.isEmpty()) {
            mViewModel.getDraftItem();
        }
    }

    private void renderLoading(boolean isLoading) {
        if (isLoading) {
            swipeRefreshLayout.setRefreshing(true);
        } else {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void renderMessage(String msg) {
        if (msg != null)
            Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onItemClick(String item_id) {
        Intent intent = new Intent(mContext, ItemEditActivity.class);
        intent.putExtra("item_id", item_id);
        this.startActivity(intent);
    }

    @Override
    public void onDeleteDraftClick(String item_id, int position) {
        deleteDialog.setPositiveButton("Delete", (dialog, which) -> {
            mViewModel.doDeleteItem(item_id, position, false);
        });
        deleteDialog.setNegativeButton("Batal", (dialog, which) -> {
            dialog.dismiss();
        });

        deleteDialog.show();

    }

    @Override
    public void onDeletePublishedClick(String item_id, int position) {
        deleteDialog.setPositiveButton("Delete", (dialog, which) -> {
            mViewModel.doDeleteItem(item_id, position, true);
        });
        deleteDialog.setNegativeButton("Batal", (dialog, which) -> {
            dialog.dismiss();
        });

        deleteDialog.show();
    }

    @Override
    public void onResume() {
        init();
        super.onResume();
    }
}
