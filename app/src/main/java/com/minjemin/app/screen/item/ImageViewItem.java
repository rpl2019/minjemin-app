package com.minjemin.app.screen.item;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.minjemin.app.R;
import com.minjemin.app.base.ViewItem;
import com.minjemin.app.models.Image;
import com.minjemin.app.utils.ViewTypeGenerator;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageViewItem extends ViewItem<ImageViewItem.Holder> {

    private Image mImage;

    public ImageViewItem(Image mImage) {
        this.mImage = mImage;
    }

    @Override
    public int getViewType() {
        return new ViewTypeGenerator(getClass().getName()).getHasCode();
    }

    @Override
    public Holder createHolder(ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new Holder(layoutInflater.inflate(R.layout.item_image, parent,false));
    }

    @Override
    public void bindHolder(Holder holder) {
        holder.bindTo(mImage);
    }

    class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_item)
        ImageView itemImage;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindTo(Image val) {
            Glide.with(itemView).load(val.getUrl()).fitCenter().into(itemImage);
        }
    }
}
