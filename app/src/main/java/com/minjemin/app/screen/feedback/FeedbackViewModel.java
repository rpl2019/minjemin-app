package com.minjemin.app.screen.feedback;

import com.minjemin.app.base.BaseErrorResponse;
import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.models.Item;
import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.utils.ErrorUtils;
import com.minjemin.app.utils.StatedViewModel;
import com.minjemin.app.utils.UtilsApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackViewModel extends StatedViewModel<FeedbackViewModel.State> {

    private BaseApiService mApiService = UtilsApi.getApiService();
    @Override
    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }
    class State {
        boolean isLoading = false;
        Status status = Status.INIT;
        Status statusLoad = Status.INIT;
        String msg = "";
        Item item;
    }

    void getItem(String id) {
        state.isLoading = true;
        state.statusLoad = Status.INIT;
        updateState();
        mApiService.getItem(id)
                .enqueue(new Callback<BaseResponse<Item>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<Item>> call, Response<BaseResponse<Item>> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.statusLoad = Status.SUCCESS;
                            state.item = response.body().getData();
                        } else {
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.msg = errorResponse.getMessage();
                            state.statusLoad = Status.ERROR;
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<Item>> call, Throwable t) {
                        state.isLoading = false;
                        state.statusLoad = Status.ERROR;
                        state.msg = t.getMessage();
                        updateState();
                    }
                });
    }

    void doSendFeedback(String id, String rating, String message) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.requestFeedback(id,rating,message)
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.status = Status.SUCCESS;
                            state.msg = response.body().getMessage();
                        } else {
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.status = Status.ERROR;
                            state.msg = errorResponse.getMessage();
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        state.msg = t.getMessage();
                        updateState();
                    }
                });
    }
}
