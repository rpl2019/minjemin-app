package com.minjemin.app.screen.chat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.minjemin.app.R;
import com.minjemin.app.models.Dialog;
import com.minjemin.app.models.Message;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.dialogs.DialogsListAdapter;
import com.stfalcon.chatkit.messages.MessagesList;

/*
 * Created by troy379 on 05.04.17.
 */
public  class DemoDialogsActivity extends Fragment
        implements DialogsListAdapter.OnDialogClickListener<Dialog>,
        DialogsListAdapter.OnDialogLongClickListener<Dialog> {

    protected ImageLoader imageLoader;
    protected DialogsListAdapter<Dialog> dialogsAdapter;


    protected void onCreateView(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        imageLoader = (imageView, url, payload) -> Glide.with(getContext()).load(url).into(imageView);
    }

    @Override
    public void onDialogLongClick(Dialog dialog) {
        Toast.makeText(getContext(),dialog.getDialogName(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDialogClick(Dialog dialog) {
        startActivity(new Intent(getContext(), MessageListActivity.class));
    }
}
