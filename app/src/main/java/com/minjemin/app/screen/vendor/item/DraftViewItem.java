package com.minjemin.app.screen.vendor.item;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.minjemin.app.R;
import com.minjemin.app.base.ViewItem;
import com.minjemin.app.models.Item;
import com.minjemin.app.utils.ViewTypeGenerator;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DraftViewItem extends ViewItem<DraftViewItem.Holder> {
    private Item item;
    private ItemListener mListener;

    public DraftViewItem(Item item, ItemListener listener) {
        this.item = item;
        this.mListener = listener;
    }

    @Override
    public int getViewType() {
        return new ViewTypeGenerator(getClass().getName()).getHasCode();
    }

    @Override
    public Holder createHolder(ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.vendor_item_barang, parent, false);
        return new Holder(view);
    }

    @Override
    public void bindHolder(Holder holder) {
        holder.bindTo(item);
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.image_item)
        ImageView imageView;
        @BindView(R.id.text_item_name)
        TextView itemName;
        @BindView(R.id.text_price_item)
        TextView itemPrice;
        String item_id;
        @BindView(R.id.text_delete)
        TextView deleteItem;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        void bindTo(Item item) {
            item_id = item.getId();
            if (item.getImages().isEmpty())
                Glide.with(itemView).load(R.drawable.ef_image_placeholder).fitCenter().into(imageView);
            else
                Glide.with(itemView).load(item.getImages().get(0).getUrl()).fitCenter().into(imageView);
            itemName.setText(item.getName());
            itemPrice.setText(item.getPriceFormat());
            deleteItem.setOnClickListener(v -> {
                mListener.onDeleteDraftClick(item_id, getAdapterPosition());
            });
        }

        @Override
        public void onClick(View v) {
            if(mListener != null) {
                mListener.onItemClick(item_id);
            }
        }
    }

    public interface ItemListener {
        void onItemClick(String item_id);
        void onDeleteDraftClick(String item_id, int position);
    }
}
