package com.minjemin.app.screen.transaksi.peminjaman;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.minjemin.app.R;
import com.minjemin.app.base.BaseRecyclerAdapter;
import com.minjemin.app.models.ItemTransaction;
import com.minjemin.app.models.Order;
import com.minjemin.app.screen.vendor.item.ItemViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PeminjamanFragment extends Fragment {

    @BindView(R.id.rv_vendor_item)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    private PeminjamanViewModel viewModel;
    private BaseRecyclerAdapter adapter;
    private Context mContext;

    public static PeminjamanFragment instance() {
        return new PeminjamanFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.vendor_fragment_item_barang, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        adapter = new BaseRecyclerAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(PeminjamanViewModel.class);
        viewModel.getState().observe(this,state -> {
            adapter.clearOnly();
            renderLoading(state.isLoading);
            renderPeminjamanItem(state.orders);
            adapter.notifyDataSetChanged();
        });
        init();
        swipeRefreshLayout.setOnRefreshListener(this::init);
    }

    void init() {
        viewModel.doGetOrders();
    }

    void renderPeminjamanItem(List<Order> items) {
        if (!items.isEmpty()) {
            adapter.clear();
            for (Order order : items) {
                adapter.addItems(new PeminjamanViewItem(order));
            }
        }
    }

    private void renderLoading(boolean isLoading) {
        if (isLoading) {
            swipeRefreshLayout.setRefreshing(true);
        } else {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

}
