package com.minjemin.app.screen.main.home;

import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.minjemin.app.R;
import com.minjemin.app.base.BaseRecyclerAdapter;
import com.minjemin.app.models.Banner;
import com.minjemin.app.models.Home;
import com.minjemin.app.models.Item;
import com.minjemin.app.screen.main.home.viewitem.MasterProductViewItem;
import com.minjemin.app.screen.main.home.viewitem.MasterSliderViewItem;
import com.minjemin.app.screen.search.SearchActivity;

import java.util.List;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    @BindView(R.id.rv_main)
    RecyclerView recyclerItem;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout refresh;
    @BindView(R.id.text_search_bar)
    TextView searchBar;
    private Context mContext;
    private BaseRecyclerAdapter adapter;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mContext = getContext();
        adapter = new BaseRecyclerAdapter();
        recyclerItem.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerItem.setAdapter(adapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mViewModel.getState().observe(this, state -> {
            adapter.clearOnly();

            // renderHeader
            // if (loading) renderLoading
            // else renderContent

            renderHome(state.newestItem, state.equipmentItem, state.bannerSliders);

            renderStatus(state.message);
            renderLoading(state.isLoading);

            adapter.notifyDataSetChanged();
        });
        mViewModel.setBannerProduct();
        init();
        refresh.setOnRefreshListener(() -> init());

        searchBar.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, SearchActivity.class);
            startActivity(intent);
        });
    }

    void init() {
        mViewModel.doGetNewest();
        mViewModel.doGetEquipment();
    }

    void renderStatus(String msg) {
        if (!msg.equals("")) {
            Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
        }
    }

    void renderHome(List<Home<Item>> newest, List<Home<Item>> equipment, List<Home<Banner>> banners) {
        if (!banners.isEmpty()) {
            for (Home<Banner> bn : banners)
                adapter.addItems(new MasterSliderViewItem(bn));
        }

        if (!newest.isEmpty()) {
            for (Home<Item> newItem : newest)
                adapter.addItems(new MasterProductViewItem(newItem));
        }

        if (!equipment.isEmpty()) {
            for (Home<Item> eq : equipment)
                adapter.addItems(new MasterProductViewItem(eq));
        }
    }

    private void renderLoading(boolean isLoading) {
        if (isLoading) {
            refresh.setRefreshing(true);
        } else {
            refresh.setRefreshing(false);
        }
    }


}
