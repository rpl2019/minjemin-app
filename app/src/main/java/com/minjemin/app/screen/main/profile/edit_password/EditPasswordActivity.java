package com.minjemin.app.screen.main.profile.edit_password;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.minjemin.app.R;
import com.minjemin.app.data.AppState;
import com.minjemin.app.models.User;
import com.minjemin.app.screen.forgot_password.ForgotPasswordActivity;

import java.util.List;


public class EditPasswordActivity extends AppCompatActivity {
    private EditPasswordViewModel viewModel;
    private static final int RC_FORGOT_PASSWORD = 200;
    @BindView(R.id.textCurrentPass)
    EditText mCurrentPassword;
    @BindView(R.id.textNewPass)
    EditText mNewPassword;
    @BindView(R.id.textConfirmPass)
    EditText mConfirmPassword;
    @BindView(R.id.buttonChangePassword)
    Button mChangePaswordBtn;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.input_layout_old_pass)
    TextInputLayout currentPassLayout;
    @BindView(R.id.input_layout_new_pass)
    TextInputLayout newPassLayout;
    @BindView(R.id.input_layout_confirm_pass)
    TextInputLayout confirmPassLayout;
    @BindView(R.id.tvForgetPass)
    TextView mForgetPass;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_password);
        ButterKnife.bind(this);
        mContext = this;
        init();
    }


    private void init() {
        viewModel = ViewModelProviders.of(this).get(EditPasswordViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderErrors(state.errors);
            renderChangePassword(state.message);
            renderStatus(state.status);
        });

        changePassword();
        changeListenerLayoutText();
        forgetPassword();
    }

    private void changePassword() {
        mChangePaswordBtn.setOnClickListener(v -> {
            viewModel.doChangePassword(AppState.getInstance().getUser().getId(),
                    mCurrentPassword.getText().toString(),
                    mNewPassword.getText().toString(),
                    mConfirmPassword.getText().toString());
        });
    }


    private void renderChangePassword(String msg) {
        if (!msg.isEmpty())
            Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    private void renderStatus(EditPasswordViewModel.Status status) {
        if (status == EditPasswordViewModel.Status.SUCCESS)
            finish();
    }

    private void renderErrors(User.ErrorChangePassword error) {
        if (error.getCurrentPassword() != null)
            currentPassLayout.setError(error.getCurrentPassword());
        else currentPassLayout.setError(null);

        if (error.getNewPassword() != null) newPassLayout.setError(error.getNewPassword());
        else newPassLayout.setError(null);

        if (error.getConfirmPassword() != null)
            confirmPassLayout.setError(error.getConfirmPassword());
        else confirmPassLayout.setError(null);

    }

    private void renderLoading(boolean isLoading) {
        mChangePaswordBtn.setEnabled(!isLoading);
        progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    private void changeListenerLayoutText() {
        mCurrentPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                currentPassLayout.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mNewPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                newPassLayout.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                confirmPassLayout.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void forgetPassword() {
        mForgetPass.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, ForgotPasswordActivity.class);
            intent.putExtra("FROM", getClass().getName());
            startActivityForResult(intent, RC_FORGOT_PASSWORD);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RC_FORGOT_PASSWORD) {
            Log.d(getClass().getName(), "FORGOT PASSWORD SUCCESS");
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
