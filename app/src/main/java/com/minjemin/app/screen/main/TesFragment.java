package com.minjemin.app.screen.main;

import com.minjemin.app.screen.main.home.MainViewModel;

public class TesFragment extends BaseRecyclerViewFragment<MainViewModel, MainViewModel.State> {

    @Override
    Class<MainViewModel> getViewModelClass() { return MainViewModel.class; }

    void renderHeader(MainViewModel.State state) {

    }

    void renderLoading(MainViewModel.State state) {

    }

    void renderContent(MainViewModel.State state) {

    }

    @Override
    void render(MainViewModel.State state) {
        adapter.clearOnly();
        renderHeader(state);

        if (state.isLoading) {
            renderLoading(state);
        } else {
            renderContent(state);
        }

        adapter.notifyDataSetChanged();
    }
}
