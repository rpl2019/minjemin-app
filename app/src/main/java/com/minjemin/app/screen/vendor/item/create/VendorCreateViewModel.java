package com.minjemin.app.screen.vendor.item.create;

import android.util.Log;

import com.google.gson.JsonObject;
import com.minjemin.app.base.BaseErrorResponse;
import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.models.Category;
import com.minjemin.app.models.Image;
import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.utils.ErrorUtils;
import com.minjemin.app.utils.StatedViewModel;
import com.minjemin.app.utils.UtilsApi;

import org.json.JSONObject;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Iterator;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VendorCreateViewModel extends StatedViewModel<VendorCreateViewModel.State> {

    private BaseApiService mApiService = UtilsApi.getApiService();

    @Override
    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        boolean isCreated = false;
        Status status = Status.INIT;
        ArrayList<Category> categories;
        Image image;
        ArrayList<Image> listImage;
        String message = null;
        String[] errorMessage;
    }


    void doGetCategories() {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.getCategories()
                .enqueue(new Callback<BaseResponse<ArrayList<Category>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<ArrayList<Category>>> call, Response<BaseResponse<ArrayList<Category>>> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.status = Status.SUCCESS;
                            state.categories = response.body().getData();
                        } else {
                            state.status = Status.ERROR;
                            Log.d("GetCategories", "Error : " + response.message());
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<ArrayList<Category>>> call, Throwable t) {
                        state.status = Status.ERROR;
                        updateState();
                        Log.d("GetCategories", "Error : " + t.getMessage());
                    }
                });
    }

    void doUploadImage(MultipartBody.Part image) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.uploadImage(image)
                .enqueue(new Callback<BaseResponse<Image>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<Image>> call, Response<BaseResponse<Image>> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.status = Status.SUCCESS;
                            state.image = response.body().getData();
                            addImage(state.image);
                            state.message = response.body().getMessage();
                        } else {
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.status = Status.ERROR;
                            Log.d("ERRORS", response.errorBody().toString());
                            state.message = errorResponse.getMessage();
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<Image>> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        state.message = t.getMessage();
                        updateState();
                        Log.d("UploadImage", "Errors : " + t.getMessage());
                    }
                });
    }

    void doStoreItem(String name, String description, String price, String category_id, String status, String... images) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.storeItem(name, description, price, category_id, status, images)
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.status = Status.SUCCESS;
                            state.message = response.body().getMessage();
                            state.isCreated = true;
                        } else {
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.status = Status.ERROR;
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        state.message = t.getMessage();
                        updateState();
                        Log.d("StoreBarang", "Errors : " + t.getMessage());
                    }
                });
    }

    void doDeleteImage(String id, int position) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.deleteImage(id)
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.status = Status.SUCCESS;
                            state.message = response.body().getMessage();
                            state.listImage.remove(position);
                        } else {
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.status = Status.ERROR;
                            state.message = errorResponse.getMessage();
                        }

                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        state.message = t.getMessage();
                        updateState();
                        Log.d("DeleteImage", "Errors : " + t.getMessage());
                    }
                });
    }

    private void addImage(Image image) {
        if (state.listImage == null) {
            state.listImage = new ArrayList<>();
        }
        state.listImage.add(image);
        updateState();
    }

    private void initiateErrorMessage(int length) {
        if (state.errorMessage == null) {
            state.errorMessage = new String[length];
        }
        updateState();
    }
}
