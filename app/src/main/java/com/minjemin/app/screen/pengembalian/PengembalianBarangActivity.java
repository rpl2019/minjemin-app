package com.minjemin.app.screen.pengembalian;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;

import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Context;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.minjemin.app.R;

import java.util.List;

public class PengembalianBarangActivity extends AppCompatActivity {

    private PengembalianViewModel viewModel;
    @BindView(R.id.select_metode)
    Spinner spinnerMetode;
    @BindView(R.id.text_alamat)
    EditText textAlamat;
    @BindView(R.id.tv_tanggal)
    TextView tvTanggal;
    @BindView(R.id.tv_waktu)
    TextView tvWaktu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.button_pengembalian)
    Button btnPengembalian;
    String id;
    private Context mContext;
    private ArrayAdapter<String> spinnerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengembalian);
        ButterKnife.bind(this);
        mContext = this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Kembalikan Barang");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        id = getIntent().getStringExtra("orderId");
        btnPengembalian.setOnClickListener(v -> {
            if (textAlamat.getText().toString().isEmpty()){
                textAlamat.setError("Wajib diisi");
                return;
            }
            viewModel.doReturn(id, textAlamat.getText().toString());
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void init() {
        viewModel = ViewModelProviders.of(this).get(PengembalianViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderCourier(state.courierList);
            renderStatus(state.status);
        });
        viewModel.setupCourier();
    }

    private void renderLoading(boolean isLoading) {
        if (isLoading) {
            Toast.makeText(getApplicationContext(), "Returning...", Toast.LENGTH_SHORT).show();
        }
    }

    private void renderCourier(List<String> courierList) {
        spinnerAdapter = new ArrayAdapter<>(mContext,
                android.R.layout.simple_spinner_item, courierList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMetode.setAdapter(spinnerAdapter);
    }

    private void renderStatus(PengembalianViewModel.Status status) {
        if (status == PengembalianViewModel.Status.SUCCESS) {
            finish();
        }
    }

}
