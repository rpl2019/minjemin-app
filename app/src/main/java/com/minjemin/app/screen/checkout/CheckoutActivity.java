package com.minjemin.app.screen.checkout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.slybeaver.slycalendarview.SlyCalendarDialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.minjemin.app.R;
import com.minjemin.app.models.Item;
import com.minjemin.app.models.Order;
import com.minjemin.app.models.Payment;
import com.minjemin.app.screen.payment.PaymentActivity;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class CheckoutActivity extends AppCompatActivity implements SlyCalendarDialog.Callback {
    @BindView(R.id.toolbar_checkout)
    Toolbar toolbar;
    @BindView(R.id.btn_calendar_picker)
    Button calendarPicker;
    @BindView(R.id.spinner_metode)
    Spinner spinnerMetode;
    @BindView(R.id.text_durasi)
    TextView textDurasi;
    @BindView(R.id.tv_nama_barang)
    TextView textItemName;
    @BindView(R.id.tv_harga)
    TextView textHarga;
    @BindView(R.id.text_alamat)
    EditText textAlamat;
    @BindView(R.id.image_barang)
    ImageView imageBarang;
    @BindView(R.id.layout_loading)
    View layoutLoading;
    @BindView(R.id.text_tgl_peminjaman)
    TextView textFromDate;
    @BindView(R.id.text_tgl_kembali)
    TextView textToDate;
    @BindView(R.id.card_date)
    CardView cardDate;
    @BindView(R.id.button_pembayaran)
    Button btnPembayaran;

    private String item_id, fromDate, toDate;
    private Context mContext;
    private CheckoutViewModel viewModel;
    private Double price, staticPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);
        mContext = this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Checkout");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void init() {
        viewModel = ViewModelProviders.of(this).get(CheckoutViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderItem(state.item, state.status);
            renderLoading(state.isLoading);
            renderAvailable(state.statusAva);
            renderOrder(state.order);
        });
        item_id = getIntent().getStringExtra("id");
        viewModel.doGetItem(item_id);
        calendarPicker.setOnClickListener(v -> {
            new SlyCalendarDialog()
                    .setSingle(false)
                    .setFirstMonday(false)
                    .setCallback(this)
                    .showNow(getSupportFragmentManager(), "CALENDAR_PICKER");
        });
        btnPembayaran.setEnabled(false);
        btnPembayaran.setOnClickListener(v -> {
            if (textAlamat.getText().toString().isEmpty()) {
                textAlamat.setError("Required Field");
                return;
            }
            viewModel.doStoreOrder(item_id, fromDate, toDate, textAlamat.getText().toString());
        });
    }

    void renderItem(Item item, CheckoutViewModel.Status status) {
        if (item != null && status == CheckoutViewModel.Status.SUCCESS) {
            textItemName.setText(item.getName());
            textHarga.setText(item.getPriceFormat());
            Glide.with(this).load(item.getImages().get(0).getUrl()).into(imageBarang);
            staticPrice = Double.parseDouble(item.getPrice());
        }
    }

    void renderOrder(Order order) {
        if (order != null) {
            Toast.makeText(mContext, viewModel.getState().getValue().orderMessage,
                    Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(mContext, PaymentActivity.class);
            intent.putExtra("number", order.getNumber());
            intent.putExtra("amount", order.getAmount());
            startActivity(intent);
            finish();
        }
    }

    void renderLoading(boolean isLoading) {
        layoutLoading.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    void renderAvailable(CheckoutViewModel.Status status) {
        if (status == CheckoutViewModel.Status.SUCCESS) {
            Toast.makeText(mContext, viewModel.getState().getValue().availability,
                    Toast.LENGTH_SHORT).show();
            btnPembayaran.setEnabled(true);
        } else if (status == CheckoutViewModel.Status.ERROR) {
            Toast.makeText(mContext, viewModel.getState().getValue().availability,
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCancelled() {

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onDataSelected(Calendar firstDate, Calendar secondDate, int hours, int minutes) {
        if (firstDate != null && secondDate != null) {
            firstDate.set(Calendar.HOUR_OF_DAY, hours);
            firstDate.set(Calendar.MINUTE, minutes);
            secondDate.set(Calendar.HOUR_OF_DAY, hours);
            secondDate.set(Calendar.MINUTE, minutes);
            int range = Days.daysBetween(new DateTime(firstDate), new DateTime(secondDate)).getDays();
            textDurasi.setText(String.valueOf(range) + " " + getString(R.string.day));
            price = staticPrice * range;
            NumberFormat numberFormat = NumberFormat.getInstance();
            textHarga.setText(numberFormat.format(price));
            if (cardDate.getVisibility() != View.VISIBLE)
                cardDate.setVisibility(View.VISIBLE);
            fromDate = new SimpleDateFormat("dd-MM-yyyy HH:mm",
                    Locale.getDefault()).format(firstDate.getTime());
            toDate = new SimpleDateFormat("dd-MM-yyyy HH:mm",
                    Locale.getDefault()).format(secondDate.getTime());
            textFromDate.setText(fromDate);
            textToDate.setText(toDate);
            viewModel.doCheckOrderExists(item_id, fromDate, toDate);
        } else {
            Toast.makeText(mContext, "Pilih Tanggal dengan benar", Toast.LENGTH_SHORT).show();
        }
    }

}
