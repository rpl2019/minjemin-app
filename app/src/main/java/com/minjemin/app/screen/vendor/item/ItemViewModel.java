package com.minjemin.app.screen.vendor.item;

import android.util.Log;

import com.minjemin.app.base.BaseErrorResponse;
import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.models.Item;
import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.utils.ErrorUtils;
import com.minjemin.app.utils.StatedViewModel;
import com.minjemin.app.utils.UtilsApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemViewModel extends StatedViewModel<ItemViewModel.State> {

    private BaseApiService mApiService = UtilsApi.getApiService();
    private ArrayList<Item> draftItem = new ArrayList<>();

    @Override
    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR;
    }

    class State {
        boolean isLoading = false;
        List<Item> publishedItems = new ArrayList<>();
        List<Item> draftItems = new ArrayList<>();
        Status status = Status.INIT;
        String message = null;
    }


    public void getPublishedItem() {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.mePublishedItem()
                .enqueue(new Callback<BaseResponse<List<Item>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Item>>> call, Response<BaseResponse<List<Item>>> response) {
                        state.isLoading = false;

                        if (response.isSuccessful()) {
                            state.status = Status.SUCCESS;
                            state.publishedItems = response.body().getData();
                        } else {
                            state.status = Status.ERROR;
                            Log.d("GetPublishedItem", "Error : " + response.message());
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<List<Item>>> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        updateState();
                    }
                });
    }

    public void getDraftItem() {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.meDraftItem()
                .enqueue(new Callback<BaseResponse<List<Item>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Item>>> call, Response<BaseResponse<List<Item>>> response) {
                        state.isLoading = false;

                        if (response.isSuccessful()) {
                            state.status = Status.SUCCESS;
                            state.draftItems = response.body().getData();
                        } else {
                            state.status = Status.ERROR;
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<List<Item>>> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        updateState();
                    }
                });
    }

    void doDeleteItem(String id, int position, boolean isPublished) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.deleteItem(id)
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.status = Status.SUCCESS;
                            state.message = response.body().getMessage();
                            if (isPublished)
                                state.publishedItems.remove(position);
                            else
                                state.draftItems.remove(position);
                        } else {
                            BaseErrorResponse errorRes = ErrorUtils.parseError(response);
                            state.status = Status.ERROR;
                            state.message = errorRes.getMessage();
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        state.message = t.getMessage();
                        updateState();
                        Log.d("DeleteItem", "Errors : " + t.getMessage());
                    }
                });
    }

}
