package com.minjemin.app.screen.extras;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.minjemin.app.R;
import com.minjemin.app.base.ViewItem;
import com.minjemin.app.models.StateEmpty;
import com.minjemin.app.utils.ViewTypeGenerator;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class EmptyItem extends ViewItem<EmptyItem.Holder> {

    private StateEmpty val;

    public EmptyItem(StateEmpty val) {
        this.val = val;
    }

    @Override
    public int getViewType() {
        return new ViewTypeGenerator(getClass().getName()).getHasCode();
    }

    @Override
    public Holder createHolder(ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.state_item_empty, parent, false);
        return new Holder(view);
    }

    @Override
    public void bindHolder(Holder holder) {
        holder.bindTo(val);
    }

    class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_empty_title)
        TextView mTitle;
        @BindView(R.id.text_empty_desc)
        TextView mDescription;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindTo(StateEmpty val) {
            mTitle.setText(val.getTitle());
            mDescription.setText(val.getDescription());
        }
    }
}
