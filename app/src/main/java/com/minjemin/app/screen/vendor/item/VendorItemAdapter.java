package com.minjemin.app.screen.vendor.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.minjemin.app.R;
import com.minjemin.app.base.BaseRecycleAdapter;
import com.minjemin.app.models.Item;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VendorItemAdapter extends BaseRecycleAdapter<Item> {

    private int viewType = 0;

    public VendorItemAdapter(Context context, ArrayList<Item> items) {
        super(context, items);
        viewType = items.isEmpty() ? 0 : 1;
    }

    @Override
    public RecyclerView.ViewHolder setViewHolder(ViewGroup parent, @Nullable int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view;

        switch (viewType) {
            case 0:
                view = layoutInflater.inflate(R.layout.state_item_empty, parent, false);
                return new EmptyHolder(view);
            case 1:
                view = layoutInflater.inflate(R.layout.vendor_item_barang, parent, false);
                return new Holder(view);
            default:
                return null;
        }
    }

    @Override
    public void onBindData(RecyclerView.ViewHolder holder, Item val) {
        Holder viewHolder = (Holder) holder;
        viewHolder.bindTo(val);
    }

    @Override
    public int getItemViewType(int position) {
        return viewType;
    }

    class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_item)
        ImageView imageView;
        @BindView(R.id.text_item_name)
        TextView itemName;
        @BindView(R.id.text_price_item)
        TextView itemPrice;

        Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindTo(Item item) {
            Glide.with(itemView).load(item.getImages().get(0).getUrl()).fitCenter().into(imageView);
            itemName.setText(item.getName());
            itemPrice.setText(item.getPriceFormat());
        }
    }

    class EmptyHolder extends RecyclerView.ViewHolder {

        public EmptyHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
