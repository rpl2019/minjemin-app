package com.minjemin.app.screen.forgot_password;

import android.util.Log;

import com.google.gson.Gson;
import com.minjemin.app.base.BaseErrorResponse;
import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.utils.ErrorUtils;
import com.minjemin.app.utils.StatedViewModel;
import com.minjemin.app.utils.UtilsApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordViewModel extends StatedViewModel<ForgotPasswordViewModel.State> {

    private BaseApiService mApiService = UtilsApi.getApiService();

    @Override
    protected State initState() {
        return new State();
    }

    enum ResetStatus {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        ResetStatus status = ResetStatus.INIT;
        String message = null;
    }

    public void doResetPassword(String email) {
        state.isLoading = true;
        state.status = ResetStatus.INIT;
        updateState();
        mApiService.resetPasswordRequest(email)
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.status = ResetStatus.SUCCESS;
                            state.message = response.body().getMessage();
                        } else {
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.status = ResetStatus.ERROR;
                            state.message = errorResponse.getErrors().get("email").getAsString();
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        state.status = ResetStatus.ERROR;
                        updateState();
                    }
                });
    }
}
