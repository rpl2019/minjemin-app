package com.minjemin.app.screen.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.minjemin.app.R;
import com.minjemin.app.utils.StatedViewModel;

public abstract class BaseStatedFragment<T extends StatedViewModel, S> extends Fragment {
    T viewModel;

    abstract Class<T> getViewModelClass();
    abstract Integer getLayout();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return  inflater.inflate(getLayout(), container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel =  ViewModelProviders.of(this).get(getViewModelClass());
        viewModel.getState().observe(this,  state -> {
            render((S) state);
        });
    }

    abstract void render(S state);
}
