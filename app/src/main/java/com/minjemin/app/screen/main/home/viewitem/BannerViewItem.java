package com.minjemin.app.screen.main.home.viewitem;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.minjemin.app.R;
import com.minjemin.app.base.ViewItem;
import com.minjemin.app.models.Banner;
import com.minjemin.app.utils.ViewTypeGenerator;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BannerViewItem extends ViewItem<BannerViewItem.Holder> {

    private Banner mBanner;

    public BannerViewItem(Banner mBanner) {
        this.mBanner = mBanner;
    }

    @Override
    public int getViewType() {
        return new ViewTypeGenerator(getClass().getName()).getHasCode();
    }

    @Override
    public Holder createHolder(ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_carousel_banner, parent, false);
        return new Holder(view);
    }

    @Override
    public void bindHolder(Holder holder) {
        holder.bind(mBanner);
    }

    class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgBanner)
        ImageView imgBanner;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bind(Banner banner) {
            Glide.with(itemView).load(banner.getImage()).fitCenter().into(imgBanner);
        }
    }
}
