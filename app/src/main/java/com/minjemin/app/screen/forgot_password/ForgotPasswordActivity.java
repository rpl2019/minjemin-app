package com.minjemin.app.screen.forgot_password;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.minjemin.app.R;
import com.minjemin.app.data.AppState;

public class ForgotPasswordActivity extends AppCompatActivity {
    private static final int RC_FORGOT_PASSWORD = 200;
    @BindView(R.id.text_email)
    EditText textEmail;
    @BindView(R.id.btn_send_reset_password)
    Button buttonSend;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    ForgotPasswordViewModel viewModel;
    Context mContext;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        mContext = this;
        viewModel = ViewModelProviders.of(this).get(ForgotPasswordViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderResetResponse(state.status, state.message);
        });
        intent = getIntent();
        initListener();
    }

    void initListener() {
        buttonSend.setOnClickListener(v -> {
            if (!validated())
                return;
            viewModel.doResetPassword(textEmail.getText().toString());
            hideKeyboard(this);
            buttonSend.setEnabled(false);
        });

        if (AppState.getInstance().isLoggedIn())
            textEmail.setText(AppState.getInstance().getUser().getEmail());
    }

    void renderLoading(Boolean isLoading) {
        buttonSend.setEnabled(!isLoading);
        progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    void renderResetResponse(ForgotPasswordViewModel.ResetStatus status, String msg) {
        if (status == ForgotPasswordViewModel.ResetStatus.SUCCESS) {
            Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
            textEmail.getText().clear();
            buttonSend.setEnabled(true);
            if (intent.hasExtra("FROM")) {
                setResult(RC_FORGOT_PASSWORD);
                finish();
            }
        } else if (status == ForgotPasswordViewModel.ResetStatus.ERROR) {
            textEmail.setError(msg);
        }
    }

    boolean validated() {
        boolean isValid = true;
        String email = textEmail.getText().toString();

        if (email.isEmpty()) {
            textEmail.setError("Email masih kosong");
            isValid = false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            textEmail.setError("Email tidak valid");
            isValid = false;
        } else {
            textEmail.setError(null);
        }


        return isValid;
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


}
