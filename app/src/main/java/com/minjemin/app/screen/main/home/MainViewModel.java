package com.minjemin.app.screen.main.home;
import android.app.Application;
import android.content.res.Resources;

import com.minjemin.app.App;
import com.minjemin.app.R;
import com.minjemin.app.base.BaseErrorResponse;
import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.data.AppState;
import com.minjemin.app.models.Banner;
import com.minjemin.app.models.Home;
import com.minjemin.app.models.Item;
import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.utils.ErrorUtils;
import com.minjemin.app.utils.StatedViewModel;
import com.minjemin.app.utils.UtilsApi;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainViewModel extends StatedViewModel<MainViewModel.State> {

    private BaseApiService mApiService = UtilsApi.getApiService();

    @Override
    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    public class State {
        public boolean isLoading = false;
        Status status = Status.INIT;
        String message = "";
        List<Banner> bannerList = new ArrayList<>();

        List<Home<Item>> newestItem = new ArrayList<>();
        List<Home<Item>> equipmentItem = new ArrayList<>();
        List<Home<Banner>> bannerSliders = new ArrayList<>();
    }

    void doGetNewest() {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();

        mApiService.getNewestItem()
                .enqueue(new Callback<BaseResponse<List<Item>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Item>>> call, Response<BaseResponse<List<Item>>> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.newestItem.clear();
                            state.status = Status.SUCCESS;
                            Home<Item> tmpItem = new Home<>();
                            tmpItem.setTitle("Terbaru");
                            tmpItem.setItemList(response.body().getData());
                            state.newestItem.add(tmpItem);
                        } else {
                            state.status = Status.ERROR;
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.message = errorResponse.getMessage();
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<List<Item>>> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        state.message = t.getMessage();
                        updateState();
                    }
                });
    }

    void doGetEquipment() {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();

        mApiService.getItemEquipment()
                .enqueue(new Callback<BaseResponse<List<Item>>>() {
                    @Override
                    public void onResponse(@NonNull Call<BaseResponse<List<Item>>> call,
                                           @NonNull Response<BaseResponse<List<Item>>> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.status = Status.SUCCESS;
                            state.equipmentItem.clear();
                            Home<Item> tmpItem = new Home<>();
                            tmpItem.setTitle("Equipment");
                            tmpItem.setItemList(response.body().getData());
                            state.equipmentItem.add(tmpItem);
                        } else {
                            state.status = Status.ERROR;
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.message = errorResponse.getMessage();
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<List<Item>>> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        state.message = t.getMessage();
                        updateState();
                    }
                });
    }

    public void setBannerProduct() {
        setBanner();
        Home<Banner> tmpBanner = new Home<>();
        tmpBanner.setTitle("Promo");
        tmpBanner.setItemList(state.bannerList);
        state.bannerSliders.add(tmpBanner);
    }

    private void setBanner() {
        state.bannerList.add(new Banner("Puncak Badai Uang", "https://s3.bukalapak.com/uploads/flash_banner/81573/homepage_banner/s-834-352/Banner_A-B_Test.jpg.webp"));
        state.bannerList.add(new Banner("Kidzania", "https://s3.bukalapak.com/uploads/flash_banner/87473/homepage_banner/s-834-352/Banner_A-B_Test_Copy-kidza.jpg.webp"));
        state.bannerList.add(new Banner("Special Ladies", "https://s3.bukalapak.com/uploads/flash_banner/87373/homepage_banner/s-834-352/Banner_A-B_Testjulie.jpg.webp"));
        updateState();
    }

}
