package com.minjemin.app.screen.item;

import com.minjemin.app.base.BaseErrorResponse;
import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.models.Image;
import com.minjemin.app.models.Item;
import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.screen.main.home.MainViewModel;
import com.minjemin.app.utils.ErrorUtils;
import com.minjemin.app.utils.StatedViewModel;
import com.minjemin.app.utils.UtilsApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ItemDetailViewModel extends StatedViewModel<ItemDetailViewModel.State> {

    private BaseApiService mApiService = UtilsApi.getApiService();

    @Override
    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR,
    }

    class State {
        List<Item> itemNewest = new ArrayList<>();
        boolean isLoading = false;
        Status status = Status.INIT;
        Status statusNewest = Status.INIT;
        Item item = new Item();
        String message = null;
    }

    void doGetItem(String id) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();

        mApiService.getItem(id)
                .enqueue(new Callback<BaseResponse<Item>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<Item>> call, Response<BaseResponse<Item>> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.item = response.body().getData();
                            state.status = Status.SUCCESS;
                        } else {
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.status = Status.ERROR;
                            state.message = errorResponse.getMessage();
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<Item>> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        state.message = t.getMessage();
                        updateState();
                    }
                });

    }

    void doGetNewest() {
        state.isLoading = true;
        state.statusNewest = Status.INIT;
        updateState();

        mApiService.getNewestItem()
                .enqueue(new Callback<BaseResponse<List<Item>>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Item>>> call, Response<BaseResponse<List<Item>>> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.statusNewest = Status.SUCCESS;
                            state.itemNewest = response.body().getData();
                        } else {
                            state.statusNewest = Status.ERROR;
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.message = errorResponse.getMessage();
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<List<Item>>> call, Throwable t) {
                        state.isLoading = false;
                        state.statusNewest = Status.ERROR;
                        state.message = t.getMessage();
                        updateState();
                    }
                });
    }

}
