package com.minjemin.app.screen.main.profile.setting_preference;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.minjemin.app.R;
import com.minjemin.app.data.AppState;
import com.minjemin.app.screen.main.MainActivity;
import com.minjemin.app.screen.main.profile.ProfileFragment;
import com.minjemin.app.utils.LocaleHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingPreferenceActivity extends AppCompatActivity {
    @BindView(R.id.switch_change_lg)
    Switch switch_change_lg;
    @BindView(R.id.buttonSimpan)
    Button btnSave;
    @BindView(R.id.tv_bahasa)
    TextView tv_bahasa;
    @BindView(R.id.toolbar_ganti_bahasa)
    Toolbar toolbar;
    private String mLanguageCode = "en";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_preference);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(getString(R.string.ganti_bahasa));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        switch_change_lg.setChecked(prefs.getBoolean("changeLanguage", switch_change_lg.isChecked()));
        switch_change_lg.setOnCheckedChangeListener((buttonView, isChecked) -> changeLanguage());
        btnSave.setOnClickListener(v -> {
            Toast.makeText(getApplicationContext(),"Language Changed",Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, MainActivity.class));
            recreate();
        });
    }

    public void changeLanguage() {
        SharedPreferences.Editor editor = getSharedPreferences(getPackageName(), MODE_PRIVATE).edit();
        editor.putBoolean("changeLanguage", switch_change_lg.isChecked());
        editor.apply();
        if (switch_change_lg.isChecked()){
            LocaleHelper.setLocale(this, mLanguageCode);
            tv_bahasa.setText(getString(R.string.bahasa));
            recreate();

        } else {
            LocaleHelper.setLocale(this, LocaleHelper.SELECTED_LANGUAGE);
            tv_bahasa.setText(getString(R.string.bahasa));
            recreate();

        }
    }
}
