package com.minjemin.app.screen.main.profile.edit_profile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.minjemin.app.R;
import com.minjemin.app.models.ProfileSubMenuItem;
import com.minjemin.app.screen.main.profile.ProfileViewModel;
import com.minjemin.app.screen.main.profile.edit_alamat.EditAlamatActivity;
import com.minjemin.app.screen.main.profile.edit_password.EditPasswordActivity;
import com.minjemin.app.screen.main.profile.notification.NotificationActivity;
import com.minjemin.app.screen.main.profile.verifikasi_akun.VerifikasiAkunActivity;
import com.minjemin.app.screen.settings.SettingNotificationActivity;

import java.util.ArrayList;

public class SettingProfileActivity extends AppCompatActivity {
    @BindView(R.id.toolbar_vendor)
    Toolbar toolbar;
    @BindView(R.id.rv_setting_profile)
    RecyclerView recyclerView;

    private RecyclerView.LayoutManager layoutManager;
    private ProfileSubMenuAdapter adapter;
    private ArrayList<ProfileSubMenuItem> menu;

    private ProfileViewModel mViewModel;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_profile);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(getString(R.string.pengaturan_akun));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = this;
        menu = new ArrayList<>();

        setUp();
        adapter = new ProfileSubMenuAdapter(mContext,menu);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(adapter);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private void move(Intent intent, Boolean finish, Boolean newTask) {
        if (newTask) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        }

        startActivity(intent);

        if (finish) {
           finish();
        }
    }
    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }
    private void setUp() {
        menu.add(new ProfileSubMenuItem(getString(R.string.edit_profile), v -> {
            move(new Intent(mContext, EditProfileActivity.class), false, false);
        }));
        menu.add(new ProfileSubMenuItem(getString(R.string.verifikasi_akun), v -> {
            move(new Intent(mContext, VerifikasiAkunActivity.class), false, false);
        }));
        menu.add(new ProfileSubMenuItem(getString(R.string.alamat), v -> {
            move(new Intent(mContext, EditAlamatActivity.class), false, false);
        }));
        menu.add(new ProfileSubMenuItem(getString(R.string.password), v -> {
            move(new Intent(mContext, EditPasswordActivity.class), false, false);
        }));
        menu.add(new ProfileSubMenuItem(getString(R.string.setting_notification), v -> {
            move(new Intent(mContext, SettingNotificationActivity.class), false, false);
        }));


    }

}
