package com.minjemin.app.screen.transaksi.peminjaman.detail;

import com.minjemin.app.base.BaseErrorResponse;
import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.models.Order;
import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.utils.ErrorUtils;
import com.minjemin.app.utils.StatedViewModel;
import com.minjemin.app.utils.UtilsApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailPeminjamanViewModel extends StatedViewModel<DetailPeminjamanViewModel.State> {

    private BaseApiService mApiService = UtilsApi.getApiService();
    @Override
    protected State initState() {
        return new State();
    }

    class State {
        boolean isLoading = false;
        Order order;
        Status status = Status.INIT;
        String message = "";
    }

    enum Status {
        INIT, SUCCESS, ERROR;
    }

    public void getOrderDetail(String id) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.singleOrderMe(id)
                .enqueue(new Callback<BaseResponse<Order>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<Order>> call, Response<BaseResponse<Order>> response) {
                        state.isLoading = false;
                        if (response.isSuccessful()) {
                            state.status = Status.SUCCESS;
                            state.order = response.body().getData();
                        } else {
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            state.status = Status.ERROR;
                            state.message = errorResponse.getMessage();
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<Order>> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        state.message = t.getMessage();
                        updateState();
                    }
                });
    }
}
