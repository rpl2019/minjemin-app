package com.minjemin.app.screen.main.profile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.minjemin.app.R;
import com.minjemin.app.base.BaseRecycleAdapter;
import com.minjemin.app.models.ProfileMenuItem;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


public class ProfileAdapter extends BaseRecycleAdapter<ProfileMenuItem> {

    public ProfileAdapter(Context context, ArrayList<ProfileMenuItem> items) {
        super(context, items);
    }

    @Override
    public RecyclerView.ViewHolder setViewHolder(ViewGroup parent, @Nullable int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_profile, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindData(RecyclerView.ViewHolder holder, ProfileMenuItem val) {
        Holder viewHolder = (Holder) holder;
        viewHolder.bindTo(val);
    }

    class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_place)
        TextView title;
        @BindView(R.id.icon_place)
        ImageView icon;

        private Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindTo(ProfileMenuItem item) {
            title.setText(item.getTitle());
            icon.setImageResource(item.getIcon());
            itemView.setOnClickListener(item.getOnClick());
        }
    }
}
