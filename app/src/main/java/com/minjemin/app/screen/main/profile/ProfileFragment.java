package com.minjemin.app.screen.main.profile;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.minjemin.app.R;
import com.minjemin.app.data.AppState;
import com.minjemin.app.models.ProfileMenuItem;
import com.minjemin.app.screen.checkout.CheckoutActivity;
import com.minjemin.app.screen.login.LoginActivity;
import com.minjemin.app.screen.main.profile.edit_profile.SettingProfileActivity;
import com.minjemin.app.screen.main.profile.notification.NotificationActivity;
import com.minjemin.app.screen.main.profile.setting_preference.SettingPreferenceActivity;
import com.minjemin.app.screen.transaksi.peminjaman.detail.DetailTransaksiActivity;
import com.minjemin.app.screen.vendor.VendorActivity;
import com.minjemin.app.utils.LocaleHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment {

    //    @BindView(R.id.button_logout)
//    Button buttonLogout;
    @BindView(R.id.tvName)
    TextView textName;
    @BindView(R.id.circleImageView)
    CircleImageView avatar;
    @BindView(R.id.rv_profile)
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ProfileAdapter adapter;
    private ArrayList<ProfileMenuItem> menu;
    private ProfileViewModel mViewModel;
    private Context mContext;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mContext = getContext();
        menu = new ArrayList<>();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        init();

        // TODO: Use the ViewModel
    }

    private void init() {
//        buttonLogout.setOnClickListener(v -> {
//            if (AppState.getInstance().isLoggedIn()) {
//                AppState.getInstance().logout();
//
//                startActivity(new Intent(getContext(), LoginActivity.class)
//                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
//                getActivity().finish();
//            }
//        });
        if (mViewModel.loadUser()) {
            textName.setText(mViewModel.getUser().getName());
            if (mViewModel.getUser().getAvatar() != null) {
                Glide.with(mContext).load(mViewModel.getUser().getAvatar()).fitCenter().into(avatar);
            } else {
                avatar.setImageDrawable(Drawable.createFromPath(String.valueOf(R.drawable.ic_launcher_background)));
            }

        }

        setUp();
        adapter = new ProfileAdapter(mContext, menu);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(adapter);


    }

    private void setUp() {
        menu.add(new ProfileMenuItem(getString(R.string.item_saya), R.drawable.ic_list_blue_24dp, v -> {
            move(new Intent(mContext, VendorActivity.class),false,false);
        }));
        menu.add(new ProfileMenuItem(getString(R.string.notifikasi), R.drawable.ic_notifications_none_blue_24dp, v -> {
            move(new Intent(mContext, NotificationActivity.class),false,false);
        }));
        menu.add(new ProfileMenuItem(getString(R.string.pengaturan_akun), R.drawable.ic_settings_blue_24dp, v -> {
            move(new Intent(mContext, SettingProfileActivity.class), false, false);
        }));
        menu.add(new ProfileMenuItem(getString(R.string.bantuan), R.drawable.ic_help_outline_black_24dp, v -> {
            showToast("Bantuan Sedang Dalam Proses Develop");
            move(new Intent(mContext, DetailTransaksiActivity.class), false, false);
        }));
        menu.add(new ProfileMenuItem("Logout", R.drawable.ic_power_settings_new_black_24dp, v -> {
            logout();
        }));
        menu.add(new ProfileMenuItem(getString(R.string.ganti_bahasa) , R.drawable.ic_settings_blue_24dp, v -> {
            move(new Intent(mContext, SettingPreferenceActivity.class), false, false);
        }));
    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    private void move(Intent intent, Boolean finish, Boolean newTask) {
        if (newTask) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        }

        startActivity(intent);

        if (finish) {
            getActivity().finish();
        }
    }

    private void logout() {
        if (AppState.getInstance().isLoggedIn()) {
            AppState.getInstance().logout();
            move(new Intent(mContext, LoginActivity.class), true, true);
        }
    }

    }

