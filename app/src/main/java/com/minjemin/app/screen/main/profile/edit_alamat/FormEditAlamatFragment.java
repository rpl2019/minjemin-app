package com.minjemin.app.screen.main.profile.edit_alamat;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.minjemin.app.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FormEditAlamatFragment extends Fragment {


    public FormEditAlamatFragment newInstance() {
        return new FormEditAlamatFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form_edit_alamat, container, false);

    }


}
