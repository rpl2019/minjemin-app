package com.minjemin.app.screen.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.minjemin.app.R;
import com.minjemin.app.data.AppState;
import com.minjemin.app.screen.forgot_password.ForgotPasswordActivity;
import com.minjemin.app.screen.main.MainActivity;
import com.minjemin.app.screen.register.RegisterActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    LoginViewModel viewModel;

    public final static String TAG = "LoginActivity";

    @BindView(R.id.textEmail)
    EditText textEmail;

    @BindView(R.id.textPassword)
    EditText textPassword;

    @BindView(R.id.buttonLogin)
    Button buttonLogin;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tvRegister)
    TextView tvRegister;

    @BindView(R.id.input_layout_password)
    TextInputLayout inputLayoutPassword;

    @BindView(R.id.input_layout_email)
    TextInputLayout inputLayoutEmail;

    @BindView(R.id.tvForgetPass)
    TextView tvForgetPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        viewModel = ViewModelProviders.of(this).get(LoginViewModel.class);

        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderLoginResponse(state.status, state.message);
        });

        initListener();
    }

    void initListener() {

        if (AppState.getInstance().isLoggedIn()) {
            getView();
        }
        buttonLogin.setOnClickListener(view -> {
            if (!validated())
                return;
            viewModel.doLogin(textEmail.getText().toString(), textPassword.getText().toString());
            hideKeyboard(this);
            buttonLogin.setEnabled(false);
        });

        tvRegister.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
            startActivityForResult(intent, 0);
        });

        tvForgetPass.setOnClickListener(v -> {
            startActivity(new Intent(getApplicationContext(), ForgotPasswordActivity.class));
        });

        changeListenerLayoutText();

    }

    void renderLoading(Boolean isLoading) {
        buttonLogin.setEnabled(!isLoading);
        progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    void renderLoginResponse(LoginViewModel.LoginStatus status, String msg) {
        if (status == LoginViewModel.LoginStatus.SUCCESS) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
            getView();
        } else if (status == LoginViewModel.LoginStatus.ERROR) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        }
    }

    boolean validated() {
        boolean isValid = true;
        String email = textEmail.getText().toString();
        String password = textPassword.getText().toString();

        if (email.isEmpty()) {
            inputLayoutEmail.setError("Email masih kosong");
            isValid = false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }

        if (password.isEmpty() || password.length() < 6) {
            inputLayoutPassword.setError("Password masih kosong");
            isValid = false;
        } else {
            inputLayoutPassword.setErrorEnabled(false);
        }

        return isValid;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void getView() {
        Log.d(TAG, "isLoggedIn");
        startActivity(new Intent(this, MainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
    }

    private void changeListenerLayoutText() {
        textEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                inputLayoutEmail.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        textPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                inputLayoutPassword.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

}
