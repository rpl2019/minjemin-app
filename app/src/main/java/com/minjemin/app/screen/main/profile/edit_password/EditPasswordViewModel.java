package com.minjemin.app.screen.main.profile.edit_password;

import android.util.Log;
import com.minjemin.app.base.BaseErrorResponse;
import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.models.User;
import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.utils.ErrorUtils;
import com.minjemin.app.utils.StatedViewModel;
import com.minjemin.app.utils.UtilsApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditPasswordViewModel extends StatedViewModel<EditPasswordViewModel.State> {

    private BaseApiService mApiService = UtilsApi.getApiService();

    @Override
    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        Status status = Status.INIT;
        String message = "";
        User.ErrorChangePassword errors = new User().initErrorChangePassword();
    }

    void doChangePassword(String id, String currentPassword, String newPassword, String confirmationPassword) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();
        mApiService.changePassword(id, currentPassword, newPassword, confirmationPassword)
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        state.isLoading = false;
                        state.errors.clear();
                        updateState();
                        if (response.isSuccessful()) {
                            state.status = Status.SUCCESS;
                            state.message = response.body().getMessage();
                        } else {
                            BaseErrorResponse errorResponse = ErrorUtils.parseError(response);
                            if (errorResponse.getErrors().has("current_password"))
                                state.errors.setCurrentPassword(errorResponse.getErrors().get("current_password").getAsString());
                            if (errorResponse.getErrors().has("new_password"))
                                state.errors.setNewPassword(errorResponse.getErrors().get("new_password").getAsString());
                            if (errorResponse.getErrors().has("confirmation_password"))
                                state.errors.setConfirmPassword(errorResponse.getErrors().get("confirmation_password").getAsString());

                            state.status = Status.ERROR;
                        }
                        updateState();
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        state.isLoading = false;
                        state.status = Status.ERROR;
                        state.message = t.getMessage();
                        Log.d("ERROR", t.getMessage());
                        updateState();
                    }
                });
    }


}
