package com.minjemin.app.screen.chat;

import android.widget.ImageView;

import androidx.annotation.Nullable;

public interface ImageLoader {
    void loadImage(ImageView imageView, @Nullable String url, @Nullable Object payload);
}
