package com.minjemin.app.screen.item;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import me.relex.circleindicator.CircleIndicator2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.minjemin.app.R;
import com.minjemin.app.base.BaseRecyclerAdapter;
import com.minjemin.app.models.Image;
import com.minjemin.app.models.Item;
import com.minjemin.app.screen.checkout.CheckoutActivity;
import com.minjemin.app.screen.main.home.viewitem.ProductViewItem;

import java.util.ArrayList;
import java.util.List;

public class ItemDetailActivity extends AppCompatActivity {

    private ItemDetailViewModel viewModel;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.rv_image)
    RecyclerView recyclerView;
    @BindView(R.id.slider_indicators)
    CircleIndicator2 circleIndicator;
    @BindView(R.id.text_item_name)
    TextView itemName;
    @BindView(R.id.text_price)
    TextView itemPrice;
    @BindView(R.id.text_category)
    TextView itemCategory;
    @BindView(R.id.text_desc)
    TextView itemDesc;
    @BindView(R.id.image_vendor)
    CircleImageView imageVendor;
    @BindView(R.id.text_vendor_name)
    TextView vendorName;
    @BindView(R.id.rv_item)
    RecyclerView rvItem;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout toolbarLayout;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    private BaseRecyclerAdapter adapter;
    private Context mContext;
    private String item_id;
    private LinearSnapHelper snapHelper;
    private boolean isShow;
    private BaseRecyclerAdapter itemAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        ButterKnife.bind(this);
        mContext = this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void init() {
        item_id = getIntent().getStringExtra("id");
        snapHelper = new LinearSnapHelper();
        viewModel = ViewModelProviders.of(this).get(ItemDetailViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderItem(state.item, state.status);
            renderNewestItem(state.itemNewest, state.statusNewest);
        });
        viewModel.doGetNewest();
        viewModel.doGetItem(item_id);

        appBar.addOnOffsetChangedListener((appBarLayout, i) -> {
            isShow = true;
            int scrollRange = -1;
            if (scrollRange == -1) {
                scrollRange = appBarLayout.getTotalScrollRange();
            }
            if (scrollRange + i == 0) {
                toolbarLayout.setTitle(itemName.getText().toString());
                isShow = true;
            } else if (isShow) {
                toolbarLayout.setTitle(" ");
                isShow = false;
            }
        });
    }

    void renderLoading(boolean isLoading) {
        progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    void renderItem(Item item, ItemDetailViewModel.Status status) {
        adapter = new BaseRecyclerAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL,
                false));
        if (item != null && status == ItemDetailViewModel.Status.SUCCESS) {
            adapter.clear();
            for (Image img : item.getImages()) {
                adapter.addItems(new ImageViewItem(img));
            }
            snapHelper.attachToRecyclerView(recyclerView);
            circleIndicator.attachToRecyclerView(recyclerView, snapHelper);
            itemName.setText(item.getName());
            itemPrice.setText(item.getPriceFormat());
            itemCategory.setText(item.getCategory());
            itemDesc.setText(item.getDescription());
            vendorName.setText(item.getVendors().getName());
            float rating = item.getRating().getAverageRate() != null ?
                    Float.parseFloat(item.getRating().getAverageRate()) : 0f;
            ratingBar.setRating(rating);
            Glide.with(this).load(item.getVendors().getAvatar()).into(imageVendor);
        }
    }

    void renderNewestItem(List<Item> items, ItemDetailViewModel.Status status) {
        if (!items.isEmpty() && status == ItemDetailViewModel.Status.SUCCESS) {
            itemAdapter = new BaseRecyclerAdapter();
            for (Item it : items) {
                itemAdapter.addItems(new ProductViewItem(it));
            }
            rvItem.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL,
                    false));
            rvItem.setAdapter(itemAdapter);
        }
    }


    public void booking(View view) {
        Intent intent = new Intent(mContext, CheckoutActivity.class);
        intent.putExtra("id", item_id);
        startActivity(intent);
    }
}
