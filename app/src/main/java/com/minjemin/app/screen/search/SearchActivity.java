package com.minjemin.app.screen.search;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SearchView;

import com.minjemin.app.R;
import com.minjemin.app.base.BaseRecyclerAdapter;
import com.minjemin.app.models.Item;
import com.minjemin.app.models.StateEmpty;
import com.minjemin.app.screen.extras.EmptyItem;

import java.util.List;

public class SearchActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_search)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.text_search_bar)
    EditText searchBar;
    private SearchViewModel viewModel;
    private BaseRecyclerAdapter adapter;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    void init() {
        viewModel = ViewModelProviders.of(this).get(SearchViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderItem(state.items, state.status);
        });
        searchBar.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                viewModel.doSearch(searchBar.getText().toString());
                hideKeyboard(this);
                searchBar.clearFocus();
                return true;
            }
            return false;
        });

    }

    void renderLoading(boolean isLoading) {
        progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    void renderItem(List<Item> items, SearchViewModel.Status status) {
        adapter = new BaseRecyclerAdapter();
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        if (items.isEmpty()) {
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            adapter.addItems(new EmptyItem(new StateEmpty("Oops !", "Kata Kunci yang anda cari tidak ditemukan")));
        }
        if (!items.isEmpty() && status == SearchViewModel.Status.SUCCESS) {
            recyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
            adapter.clear();
            for (Item i : items) {
                adapter.addItems(new SearchItemView(i));
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
