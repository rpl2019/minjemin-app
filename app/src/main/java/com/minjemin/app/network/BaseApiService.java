package com.minjemin.app.network;

import com.minjemin.app.base.BaseResponse;
import com.minjemin.app.models.Category;
import com.minjemin.app.models.Image;
import com.minjemin.app.models.Invoice;
import com.minjemin.app.models.Item;
import com.minjemin.app.models.Login;
import com.minjemin.app.models.Order;
import com.minjemin.app.models.Register;
import com.minjemin.app.models.User;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface BaseApiService {
    @FormUrlEncoded
    @POST("login")
    Call<BaseResponse<Login>> loginRequest(@Field("email") String email,
                                           @Field("password") String password);


    @FormUrlEncoded
    @POST("register")
    Call<BaseResponse<Register>> registerRequest(@Field("name") String name,
                                                 @Field("username") String username,
                                                 @Field("email") String email,
                                                 @Field("password") String password,
                                                 @Field("password_confirmation") String password_confirmation,
                                                 @Field("gender") String gender);


    @FormUrlEncoded
    @POST("auth/google/login")
    Call<ResponseBody> loginGoogleRequest(@Field("email") String email,
                                          @Field("googleAuthCode") String googleAuthCode);

    //Forgot Password
    @FormUrlEncoded
    @POST("password/email")
    Call<BaseResponse> resetPasswordRequest(@Field("email") String email);

    //Get Profile
    @GET("profile")
    Call<BaseResponse<User>> getCurrentUser();

    //Get Categories
    @GET("categories")
    Call<BaseResponse<ArrayList<Category>>> getCategories();

    //Upload Image
    @Multipart
    @POST("items/image")
    Call<BaseResponse<Image>> uploadImage(@Part MultipartBody.Part image);

    //Delete Image
    @DELETE("items/image/{id}")
    Call<BaseResponse> deleteImage(@Path("id") String id);

    //Store Item
    @FormUrlEncoded
    @POST("items")
    Call<BaseResponse> storeItem(@Field("name") String name,
                                 @Field("description") String description,
                                 @Field("price") String price,
                                 @Field("category_id") String category_id,
                                 @Field("status") String status,
                                 @Field("images[]") String... images);

    @GET("items/{id}/edit")
    Call<BaseResponse<Item>> getSingleItem(@Path("id") String id);

    @DELETE("items/{id}")
    Call<BaseResponse> deleteItem(@Path("id") String id);

    @FormUrlEncoded
    @PUT("items/{id}")
    Call<BaseResponse> updateItem(@Path("id") String id,
                                  @Field("name") String name,
                                  @Field("description") String description,
                                  @Field("price") String price,
                                  @Field("category_id") String category_id,
                                  @Field("status") String status,
                                  @Field("images[]") String... images);

    //Self Item
    @GET("items")
    Call<BaseResponse<List<Item>>> mePublishedItem();

    @GET("draft/item")
    Call<BaseResponse<List<Item>>> meDraftItem();

    //Change Password
    @FormUrlEncoded
    @PUT("user/change_password/{id}")
    Call<BaseResponse> changePassword(@Path("id") String id,
                                      @Field("current_password") String current_password,
                                      @Field("new_password") String new_password,
                                      @Field("confirmation_password") String confirmation_password);

    @FormUrlEncoded
    @PUT("user/profile/{id}")
    Call<BaseResponse> updateProfile(@Path("id") String id,
                                     @Field("name") String name,
                                     @Field("email") String email,
                                     @Field("gender") String gender,
                                     @Field("phone") String phone);

    //Get Item Home
    @GET("home/newest")
    Call<BaseResponse<List<Item>>> getNewestItem();

    @GET("home/equipment")
    Call<BaseResponse<List<Item>>> getItemEquipment();

    @Multipart
    @POST("user/profile/avatar")
    Call<BaseResponse> updateAvatar(@Part MultipartBody.Part avatar);

    @GET("items/{id}")
    Call<BaseResponse<Item>> getItem(@Path("id") String id);

    @FormUrlEncoded
    @POST("search")
    Call<BaseResponse<List<Item>>> requestSearch(@Field("keyword") String keyword);

    @FormUrlEncoded
    @POST("orders/check")
    Call<BaseResponse> requestCheckOrder(@Field("item_id") String item_id,
                                         @Field("start_date") String start_date,
                                         @Field("end_date") String end_date);

    @FormUrlEncoded
    @POST("orders")
    Call<BaseResponse<Order>> requestOrder(@Field("item_id") String item_id,
                                           @Field("start_date") String start_date,
                                           @Field("end_date") String end_date,
                                           @Field("notes") String notes);

    @GET("user/invoices/")
    Call<BaseResponse<List<Invoice>>> invoiceMe();

    @GET("user/invoices/{id}")
    Call<BaseResponse<Invoice>> singleInvoiceMe(@Path("id") String id);

    @GET("orders")
    Call<BaseResponse<List<Order>>> orderMe();

    @GET("orders/{id}")
    Call<BaseResponse<Order>> singleOrderMe(@Path("id") String id);

    @Multipart
    @POST("payments")
    Call<BaseResponse> uploadTransfer(
            @Part("bank_id") RequestBody bankId,
            @Part("order_number") RequestBody orderNumber,
            @Part("bank_name") RequestBody bankName,
            @Part("account_number") RequestBody accNumber,
            @Part("account_name") RequestBody accName,
            @Part("amount") RequestBody amount,
            @Part MultipartBody.Part image);

    @FormUrlEncoded
    @POST("user/return/item")
    Call<BaseResponse> returnItem(@Field("order_id") String order_id, @Field("address") String address);

    @FormUrlEncoded
    @POST("item/feedback")
    Call<BaseResponse> requestFeedback(@Field("item_id") String item_id,
                                       @Field("rating") String rating,
                                       @Field("comment") String comment);


}
