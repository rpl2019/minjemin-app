package com.minjemin.app.base;

import android.content.Context;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseRecycleAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<T> items;

    public abstract RecyclerView.ViewHolder setViewHolder(ViewGroup parent, @Nullable int viewType);

    public abstract void onBindData(RecyclerView.ViewHolder holder, T val);

    public BaseRecycleAdapter(Context context, ArrayList<T> items) {
        this.mContext = context;
        this.items = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = setViewHolder(parent, viewType);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        onBindData(holder, items.get(position));
    }

    @Override
    public int getItemCount() {
        return items == null? 0 : items.size();
    }


    public void addItems(ArrayList<T> item) {
        items = item;
        this.notifyItemInserted(items.size());
    }

    public T getItem(int position) {
        return items.get(position);
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }
}
