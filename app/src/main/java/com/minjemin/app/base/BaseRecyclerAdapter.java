package com.minjemin.app.base;

import android.annotation.SuppressLint;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class BaseRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ViewItem> items = new ArrayList<>();
    @SuppressLint("UseSparseArrays")
    private Map<Integer, HolderCreator> holderCreatorMap = new HashMap<>();

    public void addItems(ViewItem viewItem) {
        items.add(viewItem);
        holderCreatorMap.put(viewItem.getViewType(), viewItem::createHolder);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return holderCreatorMap.get(viewType).build(parent);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        items.get(position).bindHolder(holder);
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getViewType();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    interface HolderCreator {
        RecyclerView.ViewHolder build(ViewGroup parent);
    }

    public void clearOnly() {
        items.clear();
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }
}
