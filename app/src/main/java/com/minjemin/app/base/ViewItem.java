package com.minjemin.app.base;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public abstract class ViewItem<T extends RecyclerView.ViewHolder>{
    public abstract int getViewType();
    public abstract T createHolder(ViewGroup parent);
    public abstract void bindHolder(T holder);
}
