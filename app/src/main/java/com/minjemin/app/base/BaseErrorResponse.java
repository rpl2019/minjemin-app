package com.minjemin.app.base;

import com.google.gson.JsonObject;

public class BaseErrorResponse {
    private String message;
    private JsonObject errors;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JsonObject getErrors() {
        return errors;
    }

    public void setErrors(JsonObject errors) {
        this.errors = errors;
    }
}
