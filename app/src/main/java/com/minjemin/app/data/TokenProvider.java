package com.minjemin.app.data;

public interface TokenProvider {

    void setToken(String token);

    boolean hasToken();

    String provideToken();

    void removeToken();
}
