package com.minjemin.app.utils;

import com.minjemin.app.base.BaseErrorResponse;
import com.minjemin.app.network.RetrofitClient;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtils {

    public static BaseErrorResponse parseError(Response<?> response) {
        Converter<ResponseBody, BaseErrorResponse> converter =
                RetrofitClient.getRetrofit()
                        .responseBodyConverter(BaseErrorResponse.class, new Annotation[0]);

        BaseErrorResponse error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new BaseErrorResponse();
        }

        return error;
    }
}
