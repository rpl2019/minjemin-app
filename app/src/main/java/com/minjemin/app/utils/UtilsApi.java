package com.minjemin.app.utils;

import com.minjemin.app.network.BaseApiService;
import com.minjemin.app.network.RetrofitClient;

public class UtilsApi {
    public static String BASE_URL_API = "https://minjemin.com/api/";

    public static BaseApiService getApiService() {
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }
}
