package com.minjemin.app.utils;

public class ViewTypeGenerator {

    private String viewName;

    public ViewTypeGenerator(String viewName) {
        this.viewName = viewName;
    }

    public int getHasCode() {
        return viewName.hashCode();
    }
}
